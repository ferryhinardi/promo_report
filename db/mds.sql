-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2014 at 06:12 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mds`
--
CREATE DATABASE IF NOT EXISTS `mds` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mds`;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` tinyint(4) NOT NULL,
  `department` varchar(2) NOT NULL,
  `displayalias` varchar(2) NOT NULL,
  `name` varchar(50) NOT NULL,
  `displayname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`,`department`),
  KEY `fk_CATEGORY_DEPARTMENT1_idx` (`department`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` varchar(2) NOT NULL,
  `displayalias` varchar(2) NOT NULL,
  `name` varchar(50) NOT NULL,
  `displayname` varchar(50) NOT NULL,
  `division` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_DEPARTMENT_DIVISION1_idx` (`division`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE IF NOT EXISTS `division` (
  `id` tinyint(4) NOT NULL,
  `alias` varchar(2) NOT NULL,
  `name` varchar(50) NOT NULL,
  `displayname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `alias`, `name`, `displayname`) VALUES
(1, 'AM', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `ispr`
--

CREATE TABLE IF NOT EXISTS `ispr` (
  `item` char(10) NOT NULL COMMENT '	',
  `spr` char(1) NOT NULL,
  `value` decimal(10,0) NOT NULL,
  PRIMARY KEY (`spr`,`item`),
  KEY `fk_table1_ITEM1_idx` (`item`),
  KEY `fk_table1_SPR1_idx` (`spr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `plu` char(10) NOT NULL,
  `department` varchar(2) NOT NULL,
  `category` tinyint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `tag` char(1) NOT NULL,
  `brand` int(11) NOT NULL,
  `ibp` decimal(15,4) NOT NULL,
  PRIMARY KEY (`plu`),
  KEY `fk_ITEM_BRAND1_idx` (`brand`),
  KEY `fk_ITEM_TAG1_idx` (`tag`),
  KEY `fk_ITEM_CATEGORY1_idx` (`category`,`department`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spr`
--

CREATE TABLE IF NOT EXISTS `spr` (
  `id` char(1) NOT NULL,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spr`
--

INSERT INTO `spr` (`id`, `name`) VALUES
('A', 'Harga A'),
('B', 'Harga B'),
('C', 'Harga C'),
('D', 'Harga D'),
('E', 'Harga E'),
('F', 'Harga F'),
('G', 'Harga G'),
('H', 'Harga H'),
('I', 'Harga I'),
('J', 'Harga J');

-- --------------------------------------------------------

--
-- Table structure for table `subdivision`
--

CREATE TABLE IF NOT EXISTS `subdivision` (
  `id` tinyint(4) NOT NULL,
  `alias` varchar(10) NOT NULL,
  `mdm` varchar(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `displayname` varchar(100) NOT NULL,
  `division` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_SUBDIVISION_DIVISION1_idx` (`division`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subdivision`
--

INSERT INTO `subdivision` (`id`, `alias`, `mdm`, `name`, `displayname`, `division`) VALUES
(1, 'NF3', 'ATG', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `displayname` varchar(50) NOT NULL,
  `type` char(1) NOT NULL,
  `principal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_SUPPLIER_SUPPLIER1_idx` (`principal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `displayname`, `type`, `principal`) VALUES
(10188, 'PT. ARTA BOGA CEMERLANG', 'ARTA BOGA CEMERLANG PT.', 'D', 70508),
(19333, 'PT. BAHTERA WIRANIAGA INTERNUSA ( I )', 'BAHTERA WIRANIAGA INTERNUSA PT.( I )', 'D', 70131),
(70033, 'PT. BARCLAY PRODUCT', 'BARCLAY PRODUCT PT.', 'P', NULL),
(70131, 'PT. ENZIM BIOTEKNOLOGI INTERNUSA', 'ENZIM BIOTEKNOLOGI INTERNUSA', 'P', NULL),
(70287, 'PT. MAHAKAM BETA FARMA', 'MAHAKAM BETA FARMA PT.', 'P', NULL),
(70343, 'PT. PROCTER & GAMBLE HOME PRODUC IND', 'PROCTER & GAMBLE HOME PRODUC IND', 'P', NULL),
(70346, 'PT. PARIT PADANG GLOBAL', 'PARIT PADANG GLOBAL PT.', 'P', NULL),
(70402, 'PT. SAYAP MAS UTAMA', 'SAYAP MAS UTAMA PT.', 'P', NULL),
(70424, 'PT. STERLING PRODUCTS INDONESIA', 'STERLING PRODUCTS INDONESIA PT.', 'P', NULL),
(70489, 'PT. TRIPLE ACE', 'TRIPLE ACE PT.', 'P', NULL),
(70508, 'PT. ULTRA PRIMA ABADI PT (DMO)ULTRA PRIMA ABADI (D', 'ULTRA PRIMA ABADI PT (DMO)', 'P', NULL),
(70510, 'PT. UNILEVER INDONESIA', 'UNILEVER INDONESIA PT.', 'P', NULL),
(70538, 'PT. ASIA PARAMITA INDAH', 'ASIA PARAMITA INDAH PT.', 'P', NULL),
(70622, 'PT. JOHNSON & JOHNSON INDONESIA', 'JOHNSON & JOHNSON INDONESIA PT.', 'P', NULL),
(70685, 'PT. UNI INDO UTAMA', 'UNI INDO UTAMA PT.', 'P', NULL),
(70704, 'PT. CHARMINDO MITRA RAHARJA', 'CHARMINDO MITRA RAHARJA PT.', 'P', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` char(1) NOT NULL COMMENT '	',
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`) VALUES
('', 'Blank'),
('D', 'Unknown'),
('E', 'Unknown'),
('F', 'Unknown'),
('H', 'Habiskan'),
('I', 'Unknown'),
('L', 'Unknown'),
('N', 'Retur ke Supplier'),
('O', 'Tidak boleh jual'),
('R', 'Retur ke DC'),
('S', 'Unknown'),
('T', 'Temporary');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `fk_CATEGORY_DEPARTMENT1` FOREIGN KEY (`department`) REFERENCES `department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `fk_DEPARTMENT_DIVISION1` FOREIGN KEY (`division`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ispr`
--
ALTER TABLE `ispr`
  ADD CONSTRAINT `fk_table1_ITEM1` FOREIGN KEY (`item`) REFERENCES `item` (`plu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_table1_SPR1` FOREIGN KEY (`spr`) REFERENCES `spr` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `fk_ITEM_BRAND1` FOREIGN KEY (`brand`) REFERENCES `brand` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ITEM_CATEGORY1` FOREIGN KEY (`category`, `department`) REFERENCES `category` (`id`, `department`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ITEM_TAG1` FOREIGN KEY (`tag`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `subdivision`
--
ALTER TABLE `subdivision`
  ADD CONSTRAINT `fk_SUBDIVISION_DIVISION1` FOREIGN KEY (`division`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supplier`
--
ALTER TABLE `supplier`
  ADD CONSTRAINT `fk_SUPPLIER_SUPPLIER1` FOREIGN KEY (`principal`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
