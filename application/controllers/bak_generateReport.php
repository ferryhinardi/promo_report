<?php 
	require_once (APPPATH . 'libraries/Jaspersoft/Client/Client.php');
	use \Jaspersoft\Client as Jasper;
	class Bak_generateReport extends CI_Controller {
		private $Report = '/reports/Promo_Report/MonthlyReport';
		public function index()
		{
			$client = new Jasper\Client('localhost:8080/jasperserver',// Hostname : 192.168.0.105
									'jasperadmin',					// Username
									'jasperadmin');					// Password

			$data = file_get_contents("php://input");
			$Object = json_decode($data, true);
			foreach ($Object as $key => $value) {
				# code...
				$Parameters[$key] = $value;
			}
			$controls = array(
				'id' => $Parameters['id']
			);
			$image = $client->repositoryService()->getResource("/images/Promo_Report/logo");
			$image_data = $client->repositoryService()->getBinaryFileData($image);
			$image_url = "data:image/jpeg;base64,".base64_encode($image_data);
			$Result['imageuri'] = $image_url;
			// echo "<img src=\"data:image/jpeg;base64,".base64_encode($image_data)."\"><br><h1> Example Corporation </h1>";
			try {
				$report = $client->reportService()->runReport($this->Report, $Parameters['printType'], null, null, $controls);
				$Result["Report"] = $report;
			} catch (Exception $e) {
				$Result["Error"] = $e->getMessage();
			}
			$Result = json_encode($Result);
			echo $Result;
		}

		public function download()
		{
			$url = explode('/',$_SERVER['REQUEST_URI'])[4];
			$param = explode('?', $url)[1];
			$expl = explode('&', $param);
			$reportType = $expl[0];
			$id = $expl[1];
			$client = new Jasper\Client('192.168.0.105:8080/jasperserver',// Hostname
									'jasperadmin',					// Username
									'jasperadmin');					// Password
			$controls = array('id' => $id);
			$report = $client->reportService()->runReport($this->Report, $reportType, null, null, $controls);
			$mime_types = array(
				'pdf' => 'application/pdf',
				'xls' => 'application/vnd.ms-excel',
				'csv' => 'text/csv',
				'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'rtf' => 'text/rtf',
				'odt' => 'application/vnd.oasis.opendocument.text',
				'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
				'xlsx' => 'application/vnd.ms-excel'
			);
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename=report-' . date('Y-m-d') . '.' .$reportType);
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . strlen($report));
			header('Content-Type: '.$mime_types[$reportType]);
			echo $report;
		}
	}
?>
