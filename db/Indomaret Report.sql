-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema master
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `master` ;

-- -----------------------------------------------------
-- Schema master
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `master` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema mdclm
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mdclm` ;

-- -----------------------------------------------------
-- Schema mdclm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mdclm` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema mds
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mds` ;

-- -----------------------------------------------------
-- Schema mds
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mds` DEFAULT CHARACTER SET utf8 ;
USE `master` ;

-- -----------------------------------------------------
-- Table `master`.`app_config`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `master`.`app_config` ;

CREATE TABLE IF NOT EXISTS `master`.`app_config` (
  `key` VARCHAR(255) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE INDEX `key` (`key` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `master`.`deadlineweekly`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `master`.`deadlineweekly` ;

CREATE TABLE IF NOT EXISTS `master`.`deadlineweekly` (
  `iddeadlineweekly` INT(11) NOT NULL,
  `deadline` DATETIME NOT NULL,
  `status` CHAR(1) NOT NULL COMMENT '\'A\' => Active; \'D\' =>Deactive',
  `periode` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`iddeadlineweekly`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `mds`.`division`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`division` ;

CREATE TABLE IF NOT EXISTS `mds`.`division` (
  `id` TINYINT(4) NOT NULL,
  `alias` VARCHAR(2) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `master`.`login`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `master`.`login` ;

CREATE TABLE IF NOT EXISTS `master`.`login` (
  `user_id` INT(11) NOT NULL,
  `username` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `division` TINYINT(4) NOT NULL,
  PRIMARY KEY (`user_id`),
  INDEX `FK_Division` (`division` ASC),
  CONSTRAINT `login_ibfk_1`
    FOREIGN KEY (`division`)
    REFERENCES `mds`.`division` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `master`.`weekly`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `master`.`weekly` ;

CREATE TABLE IF NOT EXISTS `master`.`weekly` (
  `idweekly` INT(11) NOT NULL,
  `existingperiod` TEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `no` TINYINT(4) NULL DEFAULT NULL,
  `plu` CHAR(8) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `deskripsi` VARCHAR(200) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `jtd` INT(11) NULL DEFAULT NULL,
  `hpp` INT(11) NULL DEFAULT NULL,
  `ppn_hpp` INT(11) NULL DEFAULT NULL,
  `harga` INT(11) NULL DEFAULT NULL,
  `ratio` FLOAT NULL DEFAULT NULL,
  `edlp` INT(11) NULL DEFAULT NULL,
  `heboh` INT(11) NULL DEFAULT NULL,
  `iklan` INT(11) NULL DEFAULT NULL,
  `hemat` INT(11) NULL DEFAULT NULL,
  `hth` INT(11) NULL DEFAULT NULL,
  `card` INT(11) NULL DEFAULT NULL,
  `harga_promo` INT(11) NULL DEFAULT NULL,
  `alokasi` FLOAT NULL DEFAULT NULL,
  `spd` FLOAT NULL DEFAULT NULL,
  `dsi` FLOAT NULL DEFAULT NULL,
  `avg_qty` DECIMAL(10,0) NULL DEFAULT NULL,
  `avg_rph` INT(11) NULL DEFAULT NULL,
  `target_qty` DECIMAL(10,0) NULL DEFAULT NULL,
  `target_rph` INT(11) NULL DEFAULT NULL,
  `keterangan` TEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `division` INT(11) NULL DEFAULT NULL,
  `iddeadlineweekly` INT(11) NULL DEFAULT NULL,
  `datein` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`idweekly`),
  INDEX `FK_DeadlineWeekly_idx` (`iddeadlineweekly` ASC),
  CONSTRAINT `FK_DeadlineWeekly`
    FOREIGN KEY (`iddeadlineweekly`)
    REFERENCES `master`.`deadlineweekly` (`iddeadlineweekly`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `mdclm` ;

-- -----------------------------------------------------
-- Table `mds`.`supplier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`supplier` ;

CREATE TABLE IF NOT EXISTS `mds`.`supplier` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(50) NOT NULL,
  `type` CHAR(1) NOT NULL,
  `principal` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_SUPPLIER_SUPPLIER1_idx` (`principal` ASC),
  CONSTRAINT `fk_SUPPLIER_SUPPLIER1`
    FOREIGN KEY (`principal`)
    REFERENCES `mds`.`supplier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mdclm`.`supplier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdclm`.`supplier` ;

CREATE TABLE IF NOT EXISTS `mdclm`.`supplier` (
  `id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mdclm`.`asupplier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdclm`.`asupplier` ;

CREATE TABLE IF NOT EXISTS `mdclm`.`asupplier` (
  `supplier` INT(11) NOT NULL,
  `alias` INT(11) NOT NULL,
  INDEX `fk_ASUPPLIER_SUPPLIER1_idx` (`supplier` ASC),
  INDEX `fk_ASUPPLIER_SUPPLIER2_idx` (`alias` ASC),
  CONSTRAINT `fk_ASUPPLIER_SUPPLIER1`
    FOREIGN KEY (`supplier`)
    REFERENCES `mds`.`supplier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ASUPPLIER_SUPPLIER2`
    FOREIGN KEY (`alias`)
    REFERENCES `mdclm`.`supplier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mdclm`.`paymentmethod`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdclm`.`paymentmethod` ;

CREATE TABLE IF NOT EXISTS `mdclm`.`paymentmethod` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mdclm`.`claim`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdclm`.`claim` ;

CREATE TABLE IF NOT EXISTS `mdclm`.`claim` (
  `id` VARCHAR(30) NOT NULL,
  `style` INT(11) NOT NULL,
  `pm` INT(11) NOT NULL,
  `cd` DATE NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `idperiod` INT(11) NOT NULL,
  `period` VARCHAR(30) NOT NULL,
  `deposit` DECIMAL(15,4) NOT NULL,
  `usage` DECIMAL(15,4) NOT NULL,
  `diff` DECIMAL(15,4) NOT NULL,
  `initiator` INT(11) NOT NULL,
  `claimed` INT(11) NOT NULL,
  `no` VARCHAR(200) NOT NULL,
  `up` VARCHAR(100) NOT NULL,
  `name1` VARCHAR(100) NOT NULL,
  `job1` VARCHAR(100) NOT NULL,
  `name2` VARCHAR(100) NOT NULL,
  `job2` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CLAIM_PAYMENTMETHOD1_idx` (`pm` ASC),
  INDEX `fk_CLAIM_SUPPLIER1_idx` (`initiator` ASC),
  INDEX `fk_CLAIM_SUPPLIER2_idx` (`claimed` ASC),
  CONSTRAINT `fk_CLAIM_PAYMENTMETHOD1`
    FOREIGN KEY (`pm`)
    REFERENCES `mdclm`.`paymentmethod` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CLAIM_SUPPLIER1`
    FOREIGN KEY (`initiator`)
    REFERENCES `mdclm`.`supplier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CLAIM_SUPPLIER2`
    FOREIGN KEY (`claimed`)
    REFERENCES `mdclm`.`supplier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mdclm`.`mechanism`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mdclm`.`mechanism` ;

CREATE TABLE IF NOT EXISTS `mdclm`.`mechanism` (
  `claim` VARCHAR(30) NOT NULL,
  `promotion` VARCHAR(10) NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  `proposal` VARCHAR(100) NOT NULL,
  `start` DATE NOT NULL,
  `end` DATE NOT NULL,
  `deposit` DECIMAL(15,4) NOT NULL,
  `usage` DECIMAL(15,4) NOT NULL,
  `diff` DECIMAL(15,4) NOT NULL,
  PRIMARY KEY (`promotion`),
  INDEX `fk_MECHANISM_CLAIM1_idx` (`claim` ASC),
  CONSTRAINT `fk_MECHANISM_CLAIM1`
    FOREIGN KEY (`claim`)
    REFERENCES `mdclm`.`claim` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `mds` ;

-- -----------------------------------------------------
-- Table `mds`.`brand`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`brand` ;

CREATE TABLE IF NOT EXISTS `mds`.`brand` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mds`.`department`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`department` ;

CREATE TABLE IF NOT EXISTS `mds`.`department` (
  `id` VARCHAR(2) NOT NULL,
  `displayalias` VARCHAR(2) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(50) NOT NULL,
  `division` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_DEPARTMENT_DIVISION1_idx` (`division` ASC),
  CONSTRAINT `fk_DEPARTMENT_DIVISION1`
    FOREIGN KEY (`division`)
    REFERENCES `mds`.`division` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mds`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`category` ;

CREATE TABLE IF NOT EXISTS `mds`.`category` (
  `id` TINYINT(4) NOT NULL,
  `department` VARCHAR(2) NOT NULL,
  `displayalias` VARCHAR(2) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`, `department`),
  INDEX `fk_CATEGORY_DEPARTMENT1_idx` (`department` ASC),
  CONSTRAINT `fk_CATEGORY_DEPARTMENT1`
    FOREIGN KEY (`department`)
    REFERENCES `mds`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mds`.`tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`tag` ;

CREATE TABLE IF NOT EXISTS `mds`.`tag` (
  `id` CHAR(1) NOT NULL COMMENT '	',
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mds`.`item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`item` ;

CREATE TABLE IF NOT EXISTS `mds`.`item` (
  `plu` CHAR(10) NOT NULL,
  `department` VARCHAR(2) NOT NULL,
  `category` TINYINT(4) NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  `tag` CHAR(1) NOT NULL,
  `brand` INT(11) NOT NULL,
  `ibp` DECIMAL(15,4) NOT NULL,
  PRIMARY KEY (`plu`),
  INDEX `fk_ITEM_BRAND1_idx` (`brand` ASC),
  INDEX `fk_ITEM_TAG1_idx` (`tag` ASC),
  INDEX `fk_ITEM_CATEGORY1_idx` (`category` ASC, `department` ASC),
  CONSTRAINT `fk_ITEM_BRAND1`
    FOREIGN KEY (`brand`)
    REFERENCES `mds`.`brand` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ITEM_CATEGORY1`
    FOREIGN KEY (`category` , `department`)
    REFERENCES `mds`.`category` (`id` , `department`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ITEM_TAG1`
    FOREIGN KEY (`tag`)
    REFERENCES `mds`.`tag` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mds`.`spr`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`spr` ;

CREATE TABLE IF NOT EXISTS `mds`.`spr` (
  `id` CHAR(1) NOT NULL,
  `name` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mds`.`ispr`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`ispr` ;

CREATE TABLE IF NOT EXISTS `mds`.`ispr` (
  `item` CHAR(10) NOT NULL COMMENT '	',
  `spr` CHAR(1) NOT NULL,
  `value` DECIMAL(10,0) NOT NULL,
  PRIMARY KEY (`spr`, `item`),
  INDEX `fk_table1_ITEM1_idx` (`item` ASC),
  INDEX `fk_table1_SPR1_idx` (`spr` ASC),
  CONSTRAINT `fk_table1_ITEM1`
    FOREIGN KEY (`item`)
    REFERENCES `mds`.`item` (`plu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_SPR1`
    FOREIGN KEY (`spr`)
    REFERENCES `mds`.`spr` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mds`.`subdivision`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mds`.`subdivision` ;

CREATE TABLE IF NOT EXISTS `mds`.`subdivision` (
  `id` TINYINT(4) NOT NULL,
  `alias` VARCHAR(10) NOT NULL,
  `mdm` VARCHAR(3) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `displayname` VARCHAR(100) NOT NULL,
  `division` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_SUBDIVISION_DIVISION1_idx` (`division` ASC),
  CONSTRAINT `fk_SUBDIVISION_DIVISION1`
    FOREIGN KEY (`division`)
    REFERENCES `mds`.`division` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `master` ;

-- -----------------------------------------------------
-- Placeholder table for view `master`.`reportclaim_vw`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `master`.`reportclaim_vw` (`id` INT, `no` INT, `name` INT, `up` INT, `title` INT, `idperiod` INT, `period` INT, `payment_method` INT, `name1` INT, `job1` INT, `name2` INT, `job2` INT);

-- -----------------------------------------------------
-- Placeholder table for view `master`.`reportclaimdetail_vw`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `master`.`reportclaimdetail_vw` (`claim` INT, `promotion` INT, `description` INT, `proposal` INT, `start` INT, `end` INT, `deposit` INT, `usage` INT, `diff` INT);

-- -----------------------------------------------------
-- procedure sp_deleteWeeklyData
-- -----------------------------------------------------

USE `master`;
DROP procedure IF EXISTS `master`.`sp_deleteWeeklyData`;

DELIMITER $$
USE `master`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteWeeklyData`(idweeklyParam INT)
BEGIN
	DELETE FROM master.weekly
    WHERE idweekly = idweeklyParam;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getDeadline
-- -----------------------------------------------------

USE `master`;
DROP procedure IF EXISTS `master`.`sp_getDeadline`;

DELIMITER $$
USE `master`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getDeadline`(iddeadlineweeklyParam INT)
BEGIN
	SELECT *, DATE_FORMAT(deadline,'%W, %d %M %Y') AS DATE, DATE_FORMAT(deadline,'%H:%i') AS TIME, 
    CASE WHEN deadline < SYSDATE() THEN 1 ELSE 0 END AS is_lock
    FROM master.deadlineweekly
    WHERE iddeadlineweekly = iddeadlineweeklyParam
    ORDER BY deadline DESC
    LIMIT 1;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getPeriodMonthly
-- -----------------------------------------------------

USE `master`;
DROP procedure IF EXISTS `master`.`sp_getPeriodMonthly`;

DELIMITER $$
USE `master`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getPeriodMonthly`()
BEGIN
	SELECT DISTINCT idperiod AS id, period FROM mdclm.claim;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getPeriodeWeekly
-- -----------------------------------------------------

USE `master`;
DROP procedure IF EXISTS `master`.`sp_getPeriodeWeekly`;

DELIMITER $$
USE `master`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getPeriodeWeekly`()
BEGIN
	SELECT iddeadlineweekly, periode 
    FROM master.deadlineweekly
    ORDER BY status, deadline;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getWeeklyData
-- -----------------------------------------------------

USE `master`;
DROP procedure IF EXISTS `master`.`sp_getWeeklyData`;

DELIMITER $$
USE `master`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getWeeklyData`(division INT, iddeadlineweekly INT)
BEGIN
	SELECT a.*
    FROM master.weekly a
    WHERE a.division = division
    AND a.iddeadlineweekly = iddeadlineweekly;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_insertWeeklyData
-- -----------------------------------------------------

USE `master`;
DROP procedure IF EXISTS `master`.`sp_insertWeeklyData`;

DELIMITER $$
USE `master`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertWeeklyData`(
	idweeklyParam INT,
    existingperiodParam TEXT,
    noParam TINYINT,
    pluParam CHAR(8),
    deskripsiParam TEXT,
    jtdParam INT,
    hppParam INT,
    ppn_hppParam INT,
    harga_jualParam INT,
    ratioParam INT,
    edlpParam INT,
    hebohParam INT,
	iklanParam INT,
	hematParam INT,
	hthParam INT,
	cardParam INT,
	harga_promoParam INT,
	alokasiParam FLOAT,
	spdParam FLOAT,
	dsiParam FLOAT,
	avg_qtyParam DECIMAL,
	avg_rphParam INT,
	target_qtyParam DECIMAL,
	target_rphParam INT,
	keteranganParam TEXT,
    iddeadlineweeklyParam INT,
	divisionParam INT
)
BEGIN
	DECLARE has_error INT;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET has_error = 1;
    
    IF (idweeklyParam IS NOT NULL AND idweeklyParam <> 0) 
    THEN
		BEGIN
			IF EXISTS (SELECT idweekly FROM master.weekly WHERE idweekly = idweeklyParam)
            THEN
				BEGIN
					-- UPDATE DATA weekly
                    UPDATE master.weekly
                    SET 
						existingperiod = existingperiodParam,
						no = noParam,
						plu = pluParam,
						deskripsi = deskripsiParam,
						jtd = jtdParam,
						hpp = hppParam,
						ppn_hpp = ppn_hppParam,
						harga = harga_jualParam,
						ratio = ratioParam,
						edlp = edlpParam,
						heboh = hebohParam,
						iklan = iklanParam,
						hemat = hematParam,
						hth = hthParam,
						card = cardParam,
						harga_promo = harga_promoParam,
						alokasi = alokasiParam,
						spd = spdParam,
						dsi = dsiParam,
						avg_qty = avg_qtyParam,
						avg_rph = avg_rphParam,
						target_qty = target_qtyParam,
						target_rph = target_rphParam,
						keterangan = keteranganParam,
						division = divisionParam,
                        iddeadlineweekly = iddeadlineweeklyParam,
                        datein = NOW()
                    WHERE idweekly = idweeklyParam;
                END;
			END IF;
        END;
	ELSE
		BEGIN
			SELECT @idweeklyParam := COALESCE(MAX(idweekly),0) + 1 AS NewIdWeekly FROM master.weekly;
			-- INSERT DATA weekly
            
			INSERT INTO master.weekly (
				idweekly,
				existingperiod,
				no,
				plu,
				deskripsi,
				jtd,
				hpp,
				ppn_hpp,
				harga,
				ratio,
				edlp,
				heboh,
				iklan,
				hemat,
				hth,
				card,
				harga_promo,
				alokasi,
				spd,
				dsi,
				avg_qty,
				avg_rph,
				target_qty,
				target_rph,
				keterangan,
				division,
                iddeadlineweekly,
				datein
			)
			VALUES(
				@idweeklyParam,
				existingperiodParam, 
				noParam,
				pluParam,
				deskripsiParam,
				jtdParam,
				hppParam,
				ppn_hppParam,
				harga_jualParam,
				ratioParam,
				edlpParam,
				hebohParam,
				iklanParam,
				hematParam,
				hthParam,
				cardParam,
				harga_promoParam,
				alokasiParam,
				spdParam,
				dsiParam,
				avg_qtyParam,
				avg_rphParam,
				target_qtyParam,
				target_rphParam,
				keteranganParam,
				divisionParam,
                iddeadlineweeklyParam,
				NOW()
			);
            
		END;
	END IF;
/*================================================================	
CALL sp_insertWeeklyData(2,'28 Nov - 2 Des',1,'ABCD1235','Test_desc2',20,20000,2000,8000,1.0,8000,2000,100,1000,5000,8000,4000,4.5,4.5,3.5,2.0,20,20,20,'Test_keterangan2',1);
SET SQL_SAFE_UPDATES = 0;
UPDATE master.weekly SET datein = NOW();
================================================================*/
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_login_system
-- -----------------------------------------------------

USE `master`;
DROP procedure IF EXISTS `master`.`sp_login_system`;

DELIMITER $$
USE `master`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login_system`(username VARCHAR(100), password VARCHAR(100))
BEGIN
	SELECT * FROM master.login a 
		JOIN mds.division b ON a.division = b.id
	WHERE a.username = username AND a.password = password;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `master`.`reportclaim_vw`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `master`.`reportclaim_vw` ;
DROP TABLE IF EXISTS `master`.`reportclaim_vw`;
USE `master`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `master`.`reportclaim_vw` AS select `a`.`id` AS `id`,`a`.`no` AS `no`,`d`.`name` AS `name`,`a`.`up` AS `up`,`a`.`title` AS `title`,`a`.`idperiod` AS `idperiod`,`a`.`period` AS `period`,`e`.`name` AS `payment_method`,`a`.`name1` AS `name1`,`a`.`job1` AS `job1`,`a`.`name2` AS `name2`,`a`.`job2` AS `job2` from ((((`mdclm`.`claim` `a` join `mdclm`.`supplier` `b` on((`a`.`claimed` = `b`.`id`))) join `mdclm`.`asupplier` `c` on((`b`.`id` = `c`.`alias`))) join `mds`.`supplier` `d` on((`c`.`supplier` = `d`.`id`))) join `mdclm`.`paymentmethod` `e` on((`a`.`pm` = `e`.`id`)));

-- -----------------------------------------------------
-- View `master`.`reportclaimdetail_vw`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `master`.`reportclaimdetail_vw` ;
DROP TABLE IF EXISTS `master`.`reportclaimdetail_vw`;
USE `master`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `master`.`reportclaimdetail_vw` AS select `mdclm`.`mechanism`.`claim` AS `claim`,`mdclm`.`mechanism`.`promotion` AS `promotion`,`mdclm`.`mechanism`.`description` AS `description`,`mdclm`.`mechanism`.`proposal` AS `proposal`,`mdclm`.`mechanism`.`start` AS `start`,`mdclm`.`mechanism`.`end` AS `end`,`mdclm`.`mechanism`.`deposit` AS `deposit`,`mdclm`.`mechanism`.`usage` AS `usage`,`mdclm`.`mechanism`.`diff` AS `diff` from `mdclm`.`mechanism`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
