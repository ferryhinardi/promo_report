var input = [], param = [], dtable_weekly, addData = [], is_lock = 0;
$(function(){
	init();
	action();
	$('#ddlPeriod').trigger('change');
	autogenerate();
});

function init() {
	$('.navbar-brand').text(company);
	/*$('.datepicker').datepicker({
		format: 'dd MM yyyy',
		autoclose: true
		/*,	startDate: '-3d'*/
	/*});*/
	validateNumber($('#txtNo'));
	validateNumber($('#txtJTD'));
	validateNumber($('#txtHpp'));
	validateNumber($('#txtHargaJual'));
	validateNumber($('#txtEDLP'));
	validateNumber($('#txtHeboh'));
	validateNumber($('#txtIklan'));
	validateNumber($('#txtHemat'));
	validateNumber($('#txtHTH'));
	validateNumber($('#txtCard'));
	validateNumber($('#txtAlokasi'));
	validateNumber($('#txtSPD'));
	validateNumber($('#txtavgQTY'));
	validateNumber($('#txtavgRPH'));
	$('#txtHppPpn').prop('disabled',true);
	$('#txtCostRatio').prop('disabled',true);
	$('#txtHargaPromo').prop('disabled',true);
	$('#txtDSI').prop('disabled',true);
	$('#txttrgtQTY').prop('disabled',true);
	$('#txttrgtRPH').prop('disabled',true);
	$('[data-toggle="popover"]').popover();

	$.ajax({
		url: base_url + 'index.php/report/getFilterPeriod',
		dataType: 'JSON',
		async: false
	}).done(function(data, textStatus, jqXHR){
		if (data.length){
			for (var i in data)
				$('#ddlPeriod').append('<option value='+data[i].iddeadlineweekly+'>'+data[i].periode+'</option>');
		}
	});
	validateDeadline($('#ddlPeriod').val());
}

function action() {
	$('#btnSaveWeekly').click(function(e){
		getDataPopup();
		$.ajax({
			url : base_url + 'index.php/report/addWeeklyData',
			type : 'POST',
			data : { input:addData },
			dataType : 'JSON',
			success : function(data, textStatus, jqXHR)
			{
				if (data == true)
				{
					$('#modal_adddata').modal('hide');
					getWeeklyData();
				}
			},
		}).done(function(data, textStatus, jqXHR){
			if (addData.idweekly == null)
				showAlert('alert-success', 'Success Insert Data');
			else
				showAlert('alert-success', 'Success Update Data');
		}).fail(function( jqXHR, textStatus ) {
			showAlert('alert-error', 'Error Insert Data');
		});
	});

	$('#ddlPeriod').change(function(e){
		param = { periode : $(this).val() };
		getWeeklyData();
		validateDeadline($(this).val());
		
	});
}

function print(element)
{
	var printType = $(element).attr('id');
	window.open('generateReport?'+printType+'&'+$('#ddlPeriod').val(),'_blank');
}

function validateDeadline(iddeadlineweeklyParam)
{
	$.ajax({
		url: base_url + 'index.php/report/getDeadline',
		dataType: 'JSON',
		type: 'POST',
		data: { iddeadlineweekly : iddeadlineweeklyParam },
		async: false
	}).done(function(data, textStatus, jqXHR){
		if (data.length){
			$('#deadline-date').text(data[0].DATE).attr('data-id',data[0].iddeadlineweekly);
			$('#deadline-time').text(data[0].TIME);
			addData.iddeadlineweekly = data[0].iddeadlineweekly;
			is_lock = data[0].is_lock;
			if (data[0].is_lock == 1)
			{
				$('#btnAdd').prop('disabled', true);
				$('#btnSaveWeekly').prop('disabled', true);
			}
			else
			{
				$('#btnAdd').prop('disabled', false);
				$('#btnSaveWeekly').prop('disabled', false);	
			}
		}
		else{
			$('#deadline-date').text('No Deadline');
			$('#deadline-time').text('');
		}
	});
}

function getWeeklyData() {
	$.ajax({
		url : base_url + 'index.php/report/getAllWeeklyData',
		data: param,
		type: 'POST',
		dataType : 'JSON',
		beforeSend : function(jqXHR, settings)
		{
			if (typeof dtable_weekly !== 'undefined') dtable_weekly.destroy();
			loadingStart();
		},
		success : function(data, textStatus, jqXHR)
		{
			loadingFinish();
		},
		async : true
	}).done(function(data, textStatus, jqXHR){
		$('.tableData').remove();
		if (data.length)
		{
			for (var i in data)
			{
				input[data[i].idweekly] = data[i];
				var tmp = $("#iTemplate").clone().attr("id",data[i].idweekly).removeClass("cloneTemplate").addClass("tableData");
				$(".iExistingPeriod", tmp).text(data[i].existingperiod);
				$(".iNo", tmp).text(data[i].no);
				$(".iPLU", tmp).text(data[i].plu);
				$(".iDeskripsi", tmp).text(data[i].deskripsi);
				$(".iJTD", tmp).text(data[i].jtd);
				$(".iHPP", tmp).text(data[i].hpp);
				$(".iPPN", tmp).text(data[i].ppn_hpp);
				$(".iHargaJual", tmp).text(data[i].harga);
				$(".iCostRatio", tmp).text(data[i].ratio);
				$(".iEDLP", tmp).text(data[i].edlp);
				$(".iHeboh", tmp).text(data[i].heboh);
				$(".iKolomIklan", tmp).text(data[i].iklan);
				$(".iSuperHemat", tmp).text(data[i].hemat);
				$(".iHTH", tmp).text(data[i].hth);
				$(".iCard", tmp).text(data[i].card);
				$(".iHargaPromo", tmp).text(data[i].harga_promo);
				$(".iAlokasi", tmp).text(data[i].alokasi);
				$(".iSPD", tmp).text(data[i].spd);
				$(".iDSI", tmp).text(data[i].dsi);
				$(".iavg_qty", tmp).text(data[i].avg_qty);
				$(".iavg_rph", tmp).text(data[i].avg_rph);
				$(".itrgt_qty", tmp).text(data[i].target_qty);
				$(".itrgt_rph", tmp).text(data[i].target_rph);
				$(".iKeterangan", tmp).text(data[i].keterangan);
				$(".Action", tmp).append('<a id="'+data[i].idweekly+'" class="icon-delete glyphicon glyphicon-trash" style="text-decoration: none;"></a>');
				
				$("#weeklyTable tbody").append(tmp);

				if (is_lock != 1)
				{
					$('.tableData').off('click').click(function(event){
						if (!$(event.target).is('td.action') && !$(event.target).is('a.icon-delete'))
						{
							$('#modal_adddata').modal('show');
							initPopup(input[this.id]);
						}
					});

					$('a.icon-delete').off('click').click(function(event){
						var popup = confirm('Are You Sure Want Delete this Data?');

						if (popup)
						{
							$.ajax({
								url:base_url + 'report/deleteWeeklyData',
								type: 'POST',
								data: { idweekly:this.id },
							}).done(function(data, textStatus, jqXHR){
								getWeeklyData();
							});
						}
					});
				}
			}
			dtable_weekly = $("#weeklyTable").DataTable({
				"info": false
			});
			initDataTable();
		}
		else
		{
			$("#weeklyTable tbody").append('<tr class="tableData"><td colspan="25">No Data Available</td></tr>');
			dtable_weekly = undefined;
		}
	});
}

function initDataTable()
{
	var tableAreaHeight = $('#weeklyTable_wrapper').height();
	var widthFilterFixed = (parseInt($('#weeklyTable_filter').width()) - parseInt($('#weeklyTable_filter label').width()));
	var widthPaginationFixed = (parseInt($('#weeklyTable_paginate').width()) - parseInt($('#weeklyTable_paginate ul.pagination').width()));
	var widthFilter = $('#weeklyTable_filter label').width();
	var widthPagination = $('#weeklyTable_paginate ul.pagination').width();
	$('#weeklyTable_filter label, #weeklyTable_paginate ul.pagination').css({
		'position':'absolute',
		'right' : 20
	});

	$('#weeklyTable_wrapper').height(tableAreaHeight);
	$('#divWeeklyTable').scroll(function(){
		$('#weeklyTable_filter label').css({
			'left': $(this).scrollLeft() + (widthFilterFixed + 10),
			'width': widthFilter
		});
		$('#weeklyTable_paginate ul.pagination').css({
			'left': $(this).scrollLeft() + (widthPaginationFixed + 10),
			'width': widthPagination
		});
	});
}

function initPopup(val)
{
	$('#txtidweekly').val(val['idweekly']);
	$('#txtExistingPeriod').val(val['existingperiod']);
	$('#txtNo').val(val['no']);
	$('#txtPLU').val(val['plu']);
	$('#txtDeskripsi').val(val['deskripsi']);
	$('#txtJTD').val(val['jtd']);
	$('#txtHpp').val(val['hpp']);
	$('#txtHppPpn').val(val['ppn_hpp']);
	$('#txtHargaJual').val(val['harga']);
	$('#txtCostRatio').val(val['ratio']);
	$('#txtEDLP').val(val['edlp']);
	$('#txtHeboh').val(val['heboh']);
	$('#txtIklan').val(val['iklan']);
	$('#txtHemat').val(val['hemat']);
	$('#txtHTH').val(val['hth']);
	$('#txtCard').val(val['card']);
	$('#txtHargaPromo').val(val['harga_promo']);
	$('#txtAlokasi').val(val['alokasi']);
	$('#txtSPD').val(val['spd']);
	$('#txtDSI').val(val['dsi']);
	$('#txtavgQTY').val(val['avg_qty']);
	$('#txtavgRPH').val(val['avg_rph']);
	$('#txttrgtQTY').val(val['target_qty']);
	$('#txttrgtRPH').val(val['target_rph']);
	$('#txtKeterangan').val(val['keterangan']);
}

function reset()
{
	$('#txtidweekly').val("");
	$('#txtExistingPeriod').val("");
	$('#txtNo').val("");
	$('#txtPLU').val("");
	$('#txtDeskripsi').val("");
	$('#txtJTD').val("");
	$('#txtHpp').val("");
	$('#txtHppPpn').val("");
	$('#txtHargaJual').val("");
	$('#txtCostRatio').val("");
	$('#txtEDLP').val("");
	$('#txtHeboh').val("");
	$('#txtIklan').val("");
	$('#txtHemat').val("");
	$('#txtHTH').val("");
	$('#txtCard').val("");
	$('#txtHargaPromo').val("");
	$('#txtAlokasi').val("");
	$('#txtSPD').val("");
	$('#txtDSI').val("");
	$('#txtavgQTY').val("");
	$('#txtavgRPH').val("");
	$('#txttrgtQTY').val("");
	$('#txttrgtRPH').val("");
	$('#txtKeterangan').val("");
}

function getDataPopup()
{
	addData = {
		idweekly: ($('#txtidweekly').val()!="") ? $('#txtidweekly').val() : null,
		existingperiod: $('#txtExistingPeriod').val(),
		no: $('#txtNo').val(),
		plu: $('#txtPLU').val(),
		deskripsi: $('#txtDeskripsi').val(),
		jtd: $('#txtJTD').val(),
		hpp: $('#txtHpp').val(),
		ppn_hpp: $('#txtHppPpn').val(),
		harga: $('#txtHargaJual').val(),
		ratio: $('#txtCostRatio').val(),
		edlp: $('#txtEDLP').val(),
		heboh: $('#txtHeboh').val(),
		iklan: $('#txtIklan').val(),
		hemat: $('#txtHemat').val(),
		hth: $('#txtHTH').val(),
		card: $('#txtCard').val(),
		harga_promo: $('#txtHargaPromo').val(),
		alokasi: $('#txtAlokasi').val(),
		spd: $('#txtSPD').val(),
		dsi: $('#txtDSI').val(),
		avg_qty: $('#txtavgQTY').val(),
		avg_rph: $('#txtavgRPH').val(),
		target_qty: $('#txttrgtQTY').val(),
		target_rph: $('#txttrgtRPH').val(),
		keterangan: $('#txtKeterangan').val(),
		iddeadlineweekly: $('#ddlPeriod').val()
	}
}

function autogenerate()
{
	var sumJenisPotonganVal = 0;
	var jual = 0;
	$('.form-group input[type=text]').change(function(e){
		if (e.target.id == 'txtEDLP' || e.target.id == 'txtHeboh' || e.target.id == 'txtIklan' || e.target.id == 'txtHemat' || e.target.id == 'txtHTH' || e.target.id == 'txtCard') {
			if (!isNaN(sumJenisPotongan())){
			 	sumJenisPotonganVal = (parseInt($('#txtEDLP').val()) + parseInt($('#txtHeboh').val()) + parseInt($('#txtIklan').val()) + parseInt($('#txtHemat').val()) + parseInt($('#txtHTH').val()) + parseInt($('#txtCard').val()));
				$('#txtCostRatio').val(parseInt(sumJenisPotonganVal) / parseInt(jual));
				$('#txtHargaPromo').val(parseInt(jual) - parseInt(sumJenisPotonganVal));
			}
		}
		if (e.target.id == 'txtHpp') {
			if ($(this).val().length !== 0)
				$('#txtHppPpn').val($(this).val() * 1.1);		
		}
		if (e.target.id == 'txtHargaJual') {
			jual = $(this).val();
			var JenisPotongan = (parseInt($('#txtEDLP').val()) + parseInt($('#txtHeboh').val()) + parseInt($('#txtIklan').val()) + parseInt($('#txtHemat').val()) + parseInt($('#txtHTH').val()) + parseInt($('#txtCard').val()));
			if (!isNaN(parseInt(JenisPotongan) / parseInt(jual)) && !isNaN(parseInt(jual) - parseInt(JenisPotongan)))
			{
				$('#txtCostRatio').val((parseFloat(JenisPotongan) / parseFloat(jual)).toFixed(2));
				$('#txtHargaPromo').val(parseInt(jual) - parseInt(JenisPotongan));
			}
		}
		if (e.target.id == 'txtAlokasi' || e.target.id == 'txtSPD')
		{
			if (!isNaN(parseFloat($('#txtAlokasi').val()) / parseFloat($('#txtSPD').val())))
			{
				$('#txtDSI').val((parseFloat($('#txtAlokasi').val()) / parseFloat($('#txtSPD').val())).toFixed(2));
			}
		}
		if (e.target.id == 'txtJTD' || e.target.id == 'txtAlokasi')
		{
			if (!isNaN(parseInt($('#txtAlokasi').val()) * parseInt($('#txtJTD').val())))
			{
				$('#txttrgtQTY').val(parseInt($('#txtAlokasi').val()) * parseInt($('#txtJTD').val()));
			}
		}
		if (!isNaN(parseInt($('#txttrgtQTY').val()) * parseInt($('#txtHargaPromo').val())))
		{
			$('#txttrgtRPH').val(parseInt($('#txttrgtQTY').val()) * parseInt($('#txtHargaPromo').val()));
		}
	});
}

function sumJenisPotongan()
{
	if (isNaN($('#txtEDLP').val()))
		return false;
	else if (isNaN($('#txtHeboh').val()))
		return false;
	else if (isNaN($('#txtIklan').val()))
		return false;
	else if (isNaN($('#txtHemat').val()))
		return false;
	else if (isNaN($('#txtHTH').val()))
		return false;
	else if (isNaN($('#txtCard').val()))
		return false;
	else
		return (parseInt($('#txtEDLP').val()) + parseInt($('#txtHeboh').val()) + parseInt($('#txtIklan').val()) + parseInt($('#txtHemat').val()) + parseInt($('#txtHTH').val()) + parseInt($('#txtCard').val()))
}

function validateNumber(el){
	el.keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) || 
			// Allow: Ctrl+C
			(e.keyCode == 67 && e.ctrlKey === true) || 
			// Allow: Ctrl+V
			(e.keyCode == 86 && e.ctrlKey === true) || 
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	el.parent().append('<span style="color:rgb(221, 214, 214);">Input Number</span>');
}

function loadingStart()
{
	$("body").append('<div id="loading" class="center" style="display:block;"><img alt="" class="iLoading"/></div>');
	$(".iLoading").attr('src', base_url + 'assets/img/loading.gif');
	$(".iLoading").css({
		'position' : 'fixed',
		'top' : $(window).height() / 2,
		'left' : $(window).width() / 2,
		'z-index' : 1000
	});
}
	
function loadingFinish()
{
	$("#loading").remove();
	$("body").css({
		opacity : 1,
		background : ''
	});
}