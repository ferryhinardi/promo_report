<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>assets/css/login.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/css/webix.css" type="text/css" media="screen">
		
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title></title>
	<script type="text/javascript">
		var base_url = "<?php echo base_url(); ?>";
		var lang_login = "<?php echo $this->lang->line('login_login'); ?>";
		var lang_welcome_message = "<?php echo $this->lang->line('login_welcome_message'); ?>";
		var lang_username = "<?php echo $this->lang->line('login_username'); ?>";
		var lang_password = "<?php echo $this->lang->line('login_password'); ?>";
		var lang_error_msg = "<?php echo $this->lang->line('login_invalid_username_and_password'); ?>";
	</script>
	<script src="<?php echo base_url();?>assets/lib/js/jquery-2.1.1.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>assets/lib/js/webix.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/login.js" type="text/javascript" ></script>
</head>
<body>
</body>
</html>
