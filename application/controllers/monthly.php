<?php

class Monthly extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(!$this->Login_m->is_logged_in()) redirect('login');

		$data = array();
		$session = $this->session->userdata('is_logged_in')[0];
		
		$data['name'] = $session['displayname'];
		$data['company'] = $this->config->item('company');
		$data['header'] = $this->load->view("partial/header");
		$data['pageContent'] = $this->load->view("partial/monthly");
		
		$this->load->view('template/main_template', $data);
	}

	public function getFilterPeriod()
	{
		if(!$this->Login_m->is_logged_in()) redirect('login');
		$this->load->model('Monthly_m', 'monthly');
		$result = $this->monthly->getFilterPeriod();
		$this->output->set_output(json_encode($result));
	}
}