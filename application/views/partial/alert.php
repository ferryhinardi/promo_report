<!-- Class: alert-success, alert-error, alert-info -->
<div id="alertMessage" class="alert" style="float:none; position:fixed; left:40px; bottom:10px;">
	<button type="button" class="close" data-dismiss="alert" style="margin-left:10px;"> x </button>
	<strong id="message"></strong>
</div>
<script type="text/javascript">
	$('#alertMessage').hide();
	function showAlert(type, message){
		$('#alertMessage').addClass(type);
		$('#alertMessage strong#message').text(message);
		$('#alertMessage').show(400);
		setTimeout(function(){ $('#alertMessage').hide(400) }, 3000);
	};
</script>