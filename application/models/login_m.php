<?php
class Login_m extends CI_Model 
{
	function login($username, $password)
	{
		$query = $this->db->query('CALL sp_login_system(?,?)',array($username, $password));
		return $query->result_array();
	}

	function is_logged_in()
	{
		return $this->session->userdata('is_logged_in')!=false;
	}
}

?>