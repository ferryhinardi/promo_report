SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `MDS` ;
CREATE SCHEMA IF NOT EXISTS `MDS` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
DROP SCHEMA IF EXISTS `MDTPO` ;
CREATE SCHEMA IF NOT EXISTS `MDTPO` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
DROP SCHEMA IF EXISTS `MDAPP` ;
CREATE SCHEMA IF NOT EXISTS `MDAPP` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
DROP SCHEMA IF EXISTS `MDCLM` ;
CREATE SCHEMA IF NOT EXISTS `MDCLM` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `MDS` ;

-- -----------------------------------------------------
-- Table `MDS`.`SUPPLIER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`SUPPLIER` ;

CREATE TABLE IF NOT EXISTS `MDS`.`SUPPLIER` (
  `id` INT NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(50) NOT NULL,
  `type` CHAR(1) NOT NULL,
  `principal` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_SUPPLIER_SUPPLIER1_idx` (`principal` ASC),
  CONSTRAINT `fk_SUPPLIER_SUPPLIER1`
    FOREIGN KEY (`principal`)
    REFERENCES `MDS`.`SUPPLIER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `MDTPO` ;
USE `MDAPP` ;
USE `MDCLM` ;

-- -----------------------------------------------------
-- Table `MDCLM`.`PAYMENTMETHOD`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDCLM`.`PAYMENTMETHOD` ;

CREATE TABLE IF NOT EXISTS `MDCLM`.`PAYMENTMETHOD` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDCLM`.`SUPPLIER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDCLM`.`SUPPLIER` ;

CREATE TABLE IF NOT EXISTS `MDCLM`.`SUPPLIER` (
  `id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDCLM`.`CLAIM`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDCLM`.`CLAIM` ;

CREATE TABLE IF NOT EXISTS `MDCLM`.`CLAIM` (
  `id` VARCHAR(30) NOT NULL,
  `style` INT NOT NULL,
  `pm` INT NOT NULL,
  `cd` DATE NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `period` VARCHAR(30) NOT NULL,
  `deposit` DECIMAL(15,4) NOT NULL,
  `usage` DECIMAL(15,4) NOT NULL,
  `diff` DECIMAL(15,4) NOT NULL,
  `initiator` INT NOT NULL,
  `claimed` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CLAIM_PAYMENTMETHOD1_idx` (`pm` ASC),
  INDEX `fk_CLAIM_PMEMOH1_idx` (`style` ASC),
  INDEX `fk_CLAIM_SUPPLIER1_idx` (`initiator` ASC),
  INDEX `fk_CLAIM_SUPPLIER2_idx` (`claimed` ASC),
  CONSTRAINT `fk_CLAIM_PAYMENTMETHOD1`
    FOREIGN KEY (`pm`)
    REFERENCES `MDCLM`.`PAYMENTMETHOD` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CLAIM_PMEMOH1`
    FOREIGN KEY (`style`)
    REFERENCES `MDCLM`.`PMEMOH` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CLAIM_SUPPLIER1`
    FOREIGN KEY (`initiator`)
    REFERENCES `MDCLM`.`SUPPLIER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CLAIM_SUPPLIER2`
    FOREIGN KEY (`claimed`)
    REFERENCES `MDCLM`.`SUPPLIER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDCLM`.`MECHANISM`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDCLM`.`MECHANISM` ;

CREATE TABLE IF NOT EXISTS `MDCLM`.`MECHANISM` (
  `claim` VARCHAR(30) NOT NULL,
  `promotion` VARCHAR(10) NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  `start` DATE NOT NULL,
  `end` DATE NOT NULL,
  `deposit` DECIMAL(15,4) NOT NULL,
  `usage` DECIMAL(15,4) NOT NULL,
  `diff` DECIMAL(15,4) NOT NULL,
  PRIMARY KEY (`claim`, `promotion`),
  INDEX `fk_MECHANISM_CLAIM1_idx` (`claim` ASC),
  CONSTRAINT `fk_MECHANISM_CLAIM1`
    FOREIGN KEY (`claim`)
    REFERENCES `MDCLM`.`CLAIM` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDCLM`.`ASUPPLIER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDCLM`.`ASUPPLIER` ;

CREATE TABLE IF NOT EXISTS `MDCLM`.`ASUPPLIER` (
  `supplier` INT NOT NULL,
  `alias` INT NOT NULL,
  INDEX `fk_ASUPPLIER_SUPPLIER1_idx` (`supplier` ASC),
  INDEX `fk_ASUPPLIER_SUPPLIER2_idx` (`alias` ASC),
  CONSTRAINT `fk_ASUPPLIER_SUPPLIER1`
    FOREIGN KEY (`supplier`)
    REFERENCES `MDS`.`SUPPLIER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ASUPPLIER_SUPPLIER2`
    FOREIGN KEY (`alias`)
    REFERENCES `MDCLM`.`SUPPLIER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `MDS` ;

-- -----------------------------------------------------
-- Data for table `MDS`.`SUPPLIER`
-- -----------------------------------------------------
START TRANSACTION;
USE `MDS`;
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70510, 'PT. UNILEVER INDONESIA', 'UNILEVER INDONESIA PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70508, 'PT. ULTRA PRIMA ABADI PT (DMO)ULTRA PRIMA ABADI (DMO)', 'ULTRA PRIMA ABADI PT (DMO)', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (10188, 'PT. ARTA BOGA CEMERLANG', 'ARTA BOGA CEMERLANG PT.', 'D', 70508);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70424, 'PT. STERLING PRODUCTS INDONESIA', 'STERLING PRODUCTS INDONESIA PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70622, 'PT. JOHNSON & JOHNSON INDONESIA', 'JOHNSON & JOHNSON INDONESIA PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70131, 'PT. ENZIM BIOTEKNOLOGI INTERNUSA', 'ENZIM BIOTEKNOLOGI INTERNUSA', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (19333, 'PT. BAHTERA WIRANIAGA INTERNUSA ( I )', 'BAHTERA WIRANIAGA INTERNUSA PT.( I )', 'D', 70131);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70402, 'PT. SAYAP MAS UTAMA', 'SAYAP MAS UTAMA PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70343, 'PT. PROCTER & GAMBLE HOME PRODUC IND', 'PROCTER & GAMBLE HOME PRODUC IND', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70704, 'PT. CHARMINDO MITRA RAHARJA', 'CHARMINDO MITRA RAHARJA PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70346, 'PT. PARIT PADANG GLOBAL', 'PARIT PADANG GLOBAL PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70538, 'PT. ASIA PARAMITA INDAH', 'ASIA PARAMITA INDAH PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70685, 'PT. UNI INDO UTAMA', 'UNI INDO UTAMA PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70489, 'PT. TRIPLE ACE', 'TRIPLE ACE PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70033, 'PT. BARCLAY PRODUCT', 'BARCLAY PRODUCT PT.', 'P', NULL);
INSERT INTO `MDS`.`SUPPLIER` (`id`, `name`, `displayname`, `type`, `principal`) VALUES (70287, 'PT. MAHAKAM BETA FARMA', 'MAHAKAM BETA FARMA PT.', 'P', NULL);

COMMIT;

