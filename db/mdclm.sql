-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2014 at 06:11 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mdclm`
--
CREATE DATABASE IF NOT EXISTS `mdclm` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mdclm`;

-- --------------------------------------------------------

--
-- Table structure for table `asupplier`
--

CREATE TABLE IF NOT EXISTS `asupplier` (
  `supplier` int(11) NOT NULL,
  `alias` int(11) NOT NULL,
  KEY `fk_ASUPPLIER_SUPPLIER1_idx` (`supplier`),
  KEY `fk_ASUPPLIER_SUPPLIER2_idx` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asupplier`
--

INSERT INTO `asupplier` (`supplier`, `alias`) VALUES
(70510, 1);

-- --------------------------------------------------------

--
-- Table structure for table `claim`
--

CREATE TABLE IF NOT EXISTS `claim` (
  `id` varchar(30) NOT NULL,
  `style` int(11) NOT NULL,
  `pm` int(11) NOT NULL,
  `cd` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `idperiod` int(11) NOT NULL,
  `period` varchar(30) NOT NULL,
  `deposit` decimal(15,4) NOT NULL,
  `usage` decimal(15,4) NOT NULL,
  `diff` decimal(15,4) NOT NULL,
  `initiator` int(11) NOT NULL,
  `claimed` int(11) NOT NULL,
  `no` varchar(200) NOT NULL,
  `up` varchar(100) NOT NULL,
  `name1` varchar(100) NOT NULL,
  `job1` varchar(100) NOT NULL,
  `name2` varchar(100) NOT NULL,
  `job2` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CLAIM_PAYMENTMETHOD1_idx` (`pm`),
  KEY `fk_CLAIM_SUPPLIER1_idx` (`initiator`),
  KEY `fk_CLAIM_SUPPLIER2_idx` (`claimed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim`
--

INSERT INTO `claim` (`id`, `style`, `pm`, `cd`, `title`, `idperiod`, `period`, `deposit`, `usage`, `diff`, `initiator`, `claimed`, `no`, `up`, `name1`, `job1`, `name2`, `job2`) VALUES
('1', 0, 1, '2014-11-10', 'Claim Promo PT. Unilever Periode Juli 2014', 72014, 'Juli 2014', '0.0000', '0.0000', '0.0000', 1, 1, 'No. 1/ MD / NF3 / ATG', 'Grace Lianti', 'Rini S.', 'NF 3 Merch Mgr.', 'Tanya Diana', 'NF Merch Sr.Mgr.'),
('2', 0, 1, '2014-11-15', '', 102014, 'Oktober 2014', '0.0000', '0.0000', '0.0000', 1, 1, 'No. 1/ MD / NF3 / ATG', '', '', '', '', ''),
('3', 0, 1, '2014-12-15', '', 72014, 'Juli 2014', '0.0000', '0.0000', '0.0000', 1, 1, 'No. 1/ MD / NF3 / ATG', 'Grace Lianti', 'Tanya Diana', 'NF Merch Sr.Mgr.', 'Rini S.', 'NF 3 Merch Mgr.');

-- --------------------------------------------------------

--
-- Table structure for table `mechanism`
--

CREATE TABLE IF NOT EXISTS `mechanism` (
  `claim` varchar(30) NOT NULL,
  `promotion` varchar(10) NOT NULL,
  `description` varchar(200) NOT NULL,
  `proposal` varchar(100) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `deposit` decimal(15,4) NOT NULL,
  `usage` decimal(15,4) NOT NULL,
  `diff` decimal(15,4) NOT NULL,
  PRIMARY KEY (`promotion`),
  KEY `fk_MECHANISM_CLAIM1_idx` (`claim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mechanism`
--

INSERT INTO `mechanism` (`claim`, `promotion`, `description`, `proposal`, `start`, `end`, `deposit`, `usage`, `diff`) VALUES
('1', '1', 'Setiap pembelian PEPSODENT PASTA GIGI EXPERT PROTEC.11735 GENTLE WHITE/PRO COMPLETE/ENAMEL SHIELD / GUM HEALTH TUB 160g  (POT SBSR Rp. 5.200)', 'xxx/NF3/TM-HAMPERS', '2014-10-25', '2014-10-29', '0.0000', '2500000.0000', '2500000.0000'),
('1', '2', 'Setiap pembelian PEPSODENT PASTA GIGI (68201) PLUS WHITENING TUB 190g (POT SBSR Rp. 3.000)', 'xxx/NF3/TM-HAMPERS/VII/2014', '2014-11-01', '2014-11-05', '550000.0000', '1000000.0000', '-450000.0000'),
('1', '3', 'Setiap pembelian PEPSODENT PASTA GIGI (68222) CENTER FRESH TUB 160g (POT SBSR Rp. 2.400)', 'xxx/NF3/TM-HAMPERS/VII/2014', '2014-11-10', '2014-11-16', '200000.0000', '150000.0000', '50000.0000');

-- --------------------------------------------------------

--
-- Table structure for table `paymentmethod`
--

CREATE TABLE IF NOT EXISTS `paymentmethod` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paymentmethod`
--

INSERT INTO `paymentmethod` (`id`, `name`) VALUES
(1, 'Potong Tagihan');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`) VALUES
(1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asupplier`
--
ALTER TABLE `asupplier`
  ADD CONSTRAINT `fk_ASUPPLIER_SUPPLIER1` FOREIGN KEY (`supplier`) REFERENCES `mds`.`supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ASUPPLIER_SUPPLIER2` FOREIGN KEY (`alias`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `claim`
--
ALTER TABLE `claim`
  ADD CONSTRAINT `fk_CLAIM_PAYMENTMETHOD1` FOREIGN KEY (`pm`) REFERENCES `paymentmethod` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_CLAIM_SUPPLIER1` FOREIGN KEY (`initiator`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_CLAIM_SUPPLIER2` FOREIGN KEY (`claimed`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mechanism`
--
ALTER TABLE `mechanism`
  ADD CONSTRAINT `fk_MECHANISM_CLAIM1` FOREIGN KEY (`claim`) REFERENCES `claim` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
