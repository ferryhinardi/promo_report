$(function(){
	webix.ui({
		view:"form",
		id:"log_form",
		elements:[
			{ view:"fieldset", label:lang_login, body:{
				width:300,
				rows:[
					{ view:"text", name:"username", label:lang_username},
					{ view:"text", name:"password", type:"password", label:lang_password},
					{ template:lang_error_msg, height : 25, id: "error_msg" },
					{ margin:5, cols:[
						{ view:"button", label:"Login", type:"htmlbutton", attributes:{ class:'webixtype_form' }, on:{
								"onItemClick": function(){
									sumbit();
								}
							}
						}
					]}
				]
			}
		}]
	}).show();

	init();

	$('body').keypress(function(e) {
		if (e.which == 13){
			sumbit();
		}
	});
});

function init()
{
	$('div[view_id=error_msg]').css({
		'border-width' : '',
		'text-align' : 'center',
		'color' : 'red'
	});
	$('div[view_id=error_msg]').hide();
	$('button:contains("Login")').attr('class','webixtype_form');
}

function sumbit()
{
	var username = $('input#username').val();
	var password = $('input#password').val();
	
	if ($.trim(username) === ""){
		$('div[view_id=error_msg] div').text('Please Input Username!');
		$('div[view_id=error_msg]').show(200);
	}
	else if (password === ""){
		$('div[view_id=error_msg] div').text('Please Input Password!');
		$('div[view_id=error_msg]').show(200);
	}
	else
	{
		$.ajax({
			url : base_url + 'index.php/login/do_login',
			type : 'POST',
			data : { username : username, password : password },
			dataType : 'JSON',
			beforeSend : function(jqXHR, settings)
			{
				$('button').html("<img class='iloading' width='25px' src='"+base_url+"assets/img/loading.gif'>");
			},
			success : function( data, textStatus, jqXHR )
			{
				$('button').html('Login');
				$('div[view_id=error_msg]').hide(200);
			},
			error : function(jqXHR, textStatus, errorThrown)
			{
				$('button').html('Login');
				$('div[view_id=error_msg]').hide(200);	
			}
		}).done(function( data, textStatus, jqXHR ){
			if (data.length)
			{
				location.reload();
			}
			else
			{
				$('div[view_id=error_msg] div').text(lang_error_msg);
				$('div[view_id=error_msg]').show(200);
			}
		}).fail(function( jqXHR, textStatus, errorThrown ) {
			console.log('Error : '+errorThrown);
			$('div[view_id=error_msg] div').text(errorThrown);
			$('div[view_id=error_msg]').show(200);
		});
	}
}