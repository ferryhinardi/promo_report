<?php
class Paging {
	public $TotalPage=0;
	public $CurrentPage=0;
	protected $Page="";
	protected $FullPage="";
	protected $HeaderHTML="";
	protected $FooterHTML="";
	protected $ReportPath="";

	function __construct() {
      
   }
	public function CreatePaging($Data='')
	{
		if(Trim($Data)!="")
		{
			$Data=str_replace("<br/>", "", $Data);
			$this->Page=$Data;
			//$this->CreateImage();
			//$this->Page=str_replace("/jasperserver", "http://localhost:8080/jasperserver", $this->Page);
			$this->Page=explode("<a name=\"JR_PAGE_ANCHOR_",$this->Page);
			if(count($this->Page)>0)
			{
				$this->TotalPage=count($this->Page);
				$this->FullPage = array_slice($this->Page, 1,$this->TotalPage);
				$this->CurrentPage=1;
				$this->HeaderHTML=$this->Page[0];
				$Footer=explode("<![if IE]>",$this->Page[$this->TotalPage-1]);
				$this->FooterHTML="<![if IE]>".str_replace("</body>
</html>"," ",$Footer[1]);
			}
		}
	}

	public function CreateImage()
	{
		//$repo = $this->Report->repositoryService()->searchResources();
		$image = $this->Report->repositoryService()->getResource($this->ReportPath);
		if(isset($image->resources))
		{
			$Source=$this->Page;
			$i=0;
			$Max=1000;
			$ImageCollection=array();
			//echo stristr($Data, 'http://localhost/Report/img_1');
			while(stristr($Source, 'http://localhost/Report/img_')!="")
			{
				$Source=stristr($Source, 'http://localhost/Report/img_'); 
				$Result= substr($Source, 0,strpos($Source, "\""));
				
				$Source=str_replace($Result, "", $Source);
				array_push($ImageCollection, $Result);
				$i++;
				if($i>=$Max)
				{
					exit;
				}
			}
			$i=0;

			foreach ($image->resources as $key => $value) 
			{
				$ImageSource=$this->Report->repositoryService()->getResource($value);
				print_r($image);
				$ImageData = $this->Report->repositoryService()->getBinaryFileData($ImageSource);
				//echo "<img src=\"data:image/jpeg;base64,".base64_encode($ImageData)."\"><br><h1> Example Corporation </h1>";	
				if(isset($ImageCollection[$i]))
				{
					$this->Page=str_replace($ImageCollection[$i], "data:image/jpeg;base64,".base64_encode($ImageData)."\"", $this->Page);
				}
				$i++;
			}
		}
	}

	public function Page($PageIndex=0)
	{
		
		if($PageIndex=="First")
		{
			$PageIndex=1;
		}
		elseif($PageIndex=="Last")
		{
			$PageIndex=($this->TotalPage-1);
		}
		else
		{
			$PageIndex=($PageIndex==0?$PageIndex=1:$PageIndex);
		}
		if(count($this->Page)>0)
		{
			include("config/config.php");
			//echo $PageIndex;
			if($PageIndex==-1)
			{
				return "<a name=\"JR_PAGE_ANCHOR_".implode("<a name=\"JR_PAGE_ANCHOR_",$this->FullPage).$this->FooterHTML;
			}
			elseif($PageIndex<count($this->Page))
			{
				
				//return $this->HeaderHTML."<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\".$theme."\letter.css\"><div class='letter'>"."<a name=\"JR_PAGE_ANCHOR_".$this->Page[$PageIndex].$this->FooterHTML."</div></body></html>";
				return "<a name=\"JR_PAGE_ANCHOR_".$this->Page[$PageIndex].$this->FooterHTML;
			}
			else
			{
				echo "Page invalid";
			}
		}
		else
		{
			return null;
		}	
	}

	public function CurrentPage()
	{
		if(count($this->Page)>0)
		{
			return $this->CurrentPage;
		}
		else
		{
			return null;
		}	
	}

	public function TotalPage()
	{
		if(count($this->Page)>0)
		{
			return $this->TotalPage;
		}
		else
		{
			return null;
		}	
	}
}
?>