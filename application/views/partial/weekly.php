<div class="well">
	<!-- <input id="startdate" class="datepicker" placeholder="Start Date">
	<span> - </span>
	<input id="enddate" class="datepicker" placeholder="End Date">
	<button type="button" id="btnLoad" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Load Data</button> -->
	<select id="ddlPeriod" class="btn btn-default dropdown-toggle">
	</select>
</div>
<div id="header-deadline" class="row">
	<div class="col-md-6">
		<h3>Deadline : <span id="deadline-date" style="font-weight: bold;"></span> <span id="deadline-time" style="font-weight: bold;"></span></h3>
	</div>
	<div class="col-md-5">
		<button type="button" class="btn btn-success btn-lg" data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" data-content="<button id='html' class='btn print' style='margin-left:5px; background-color:#238B87; color:white;' onclick='print(this)'>html</button><button id='pdf' class='btn print' style='margin-left:5px; background-color:#D10031; color:white;' onclick='print(this)'>pdf</button><button id='xlsx' class='btn print' style='margin-left:5px; background-color:#50B300; color:white;' onclick='print(this)'>Excel</button><button id='docx' class='btn print' style='margin-left:5px; background-color:#003096; color:white;' onclick='print(this)'>Word</button>" >Print Report</button>
	</div>
</div>
<hr />
<div id="divWeeklyTable" class="table-responsive content">
	<table id="weeklyTable" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th rowspan="2">Existing Period</th>
				<th rowspan="2">No</th>
				<th rowspan="2">PLU</th>
				<th rowspan="2">Deskripsi</th>
				<th rowspan="2">JTD</th>
				<th rowspan="2">HPP (EXC. HPP)</th>
				<th rowspan="2">HPP (INC. PPN)</th>
				<th rowspan="2">Harga Jual</th>
				<th rowspan="2">Cost Ratio</th>
				<th colspan="6">Jenis Potongan</th>
				<th rowspan="2">Harga Promo</th>
				<th rowspan="2">Alokasi</th>
				<th rowspan="2">SPD</th>
				<th rowspan="2">DSI</th>
				<th colspan="2">Average</th>
				<th colspan="2">Target</th>
				<th rowspan="2">Keterangan</th>
				<th rowspan="2">Action</th>
			</tr>
			<tr>
				<th>EDLP</th>
				<th>Heboh</th>
				<th>Kolom Iklan</th>
				<th>Super Hemat</th>
				<th>HTH</th>
				<th>MKT/ IDM Card</th>
				<th>QTY</th>
				<th>RPH</th>
				<th>QTY</th>
				<th>RPH</th>
			</tr>
		</thead>
		<tbody>
			<tr id="iTemplate" class="cloneTemplate">
				<td class="iExistingPeriod"></td>
				<td class="iNo"></td>
				<td class="iPLU"></td>
				<td class="iDeskripsi"></td>
				<td class="iJTD"></td>
				<td class="iHPP"></td>
				<td class="iPPN"></td>
				<td class="iHargaJual"></td>
				<td class="iCostRatio"></td>
				<td class="iEDLP"></td>
				<td class="iHeboh"></td>
				<td class="iKolomIklan"></td>
				<td class="iSuperHemat"></td>
				<td class="iHTH"></td>
				<td class="iCard"></td>
				<td class="iHargaPromo"></td>
				<td class="iAlokasi"></td>
				<td class="iSPD"></td>
				<td class="iDSI"></td>
				<td class="iavg_qty"></td>
				<td class="iavg_rph"></td>
				<td class="itrgt_qty"></td>
				<td class="itrgt_rph"></td>
				<td class="iKeterangan"></td>
				<td class="Action"></td>
			</tr>
		</tbody>
	</table>
</div>
<div class="content">
	<button id="btnAdd" type="button" class="btn btn-primary btn-lg" data-backdrop="static" onClick="reset()" data-toggle="modal" data-target="#modal_adddata" style="width:100%">Add Data</button>
</div>
<div class="modal fade" id="modal_adddata" tabindex="-1" role="dialog" aria-labelledby="modal_adddataLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Insert Data</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<input type="hidden" id="txtidweekly" />
					<div class="form-group">
						<label for="txtExistingPeriod" class="col-sm-2 control-label">Existing Period</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtExistingPeriod" placeholder="Existing Period">
						</div>
					</div>

					<div class="form-group">
						<label for="txtNo" class="col-sm-2 control-label">No</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtNo" placeholder="No">
						</div>
					</div>

					<div class="form-group">
						<label for="txtPLU" class="col-sm-2 control-label">PLU</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtPLU" placeholder="PLU" maxlength="8">
						</div>
					</div>

					<div class="form-group">
						<label for="txtDeskripsi" class="col-sm-2 control-label">Deskripsi</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtDeskripsi" placeholder="Deskripsi">
						</div>
					</div>

					<div class="form-group">
						<label for="txtJTD" class="col-sm-2 control-label">JTD</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtJTD" placeholder="JTD">
						</div>
					</div>

					<div class="form-group">
						<label for="txtHpp" class="col-sm-2 control-label">HPP (EXC. HPP)</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtHpp" placeholder="HPP (EXC. HPP)">
						</div>
					</div>

					<div class="form-group">
						<label for="txtHppPpn" class="col-sm-2 control-label">HPP (INC. PPN)</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtHppPpn" placeholder="HPP (INC. PPN)">
						</div>
					</div>

					<div class="form-group">
						<label for="txtHargaJual" class="col-sm-2 control-label">Harga Jual</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtHargaJual" placeholder="Harga Jual">
						</div>
					</div>

					<div class="form-group">
						<label for="txtCostRatio" class="col-sm-2 control-label">Cost Ratio</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtCostRatio" placeholder="Cost Ratio">
						</div>
					</div>
					<h3><span class="label label-default">Jenis Potongan</span></h3>
					<div class="form-inline">
						<div class="form-group" style="width:33%">
							<label for="txtEDLP" class="col-sm-2 control-label">EDLP</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtEDLP" placeholder="EDLP">
							</div>
						</div>

						<div class="form-group" style="width:33%">
							<label for="txtHeboh" class="col-sm-2 control-label">Heboh</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtHeboh" placeholder="Heboh">
							</div>
						</div>

						<div class="form-group" style="width:33%">
							<label for="txtIklan" class="col-sm-2 control-label">Kolom Iklan</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtIklan" placeholder="Kolom Iklan">
							</div>
						</div>

						<div class="form-group" style="width:33%">
							<label for="txtHemat" class="col-sm-2 control-label">Super Hemat</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtHemat" placeholder="Super Hemat">
							</div>
						</div>

						<div class="form-group" style="width:33%">
							<label for="txtHTH" class="col-sm-2 control-label">HTH</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtHTH" placeholder="HTH">
							</div>
						</div>

						<div class="form-group" style="width:33%">
							<label for="txtCard" class="col-sm-2 control-label">MKT/ IDM Card</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtCard" placeholder="MKT/ IDM Card">
							</div>
						</div>
					</div>
					<hr />
					<div class="form-inline">
						<div class="form-group" style="width:50%">
							<label for="txtHargaPromo" class="col-sm-2 control-label">Harga Promo</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtHargaPromo" placeholder="Harga Promo">
							</div>
						</div>

						<div class="form-group" style="width:50%">
							<label for="txtAlokasi" class="col-sm-2 control-label">Alokasi</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtAlokasi" placeholder="Alokasi">
							</div>
						</div>

						<div class="form-group" style="width:50%">
							<label for="txtSPD" class="col-sm-2 control-label">SPD</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtSPD" placeholder="SPD">
							</div>
						</div>

						<div class="form-group" style="width:50%">
							<label for="txtDSI" class="col-sm-2 control-label">DSI</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtDSI" placeholder="DSI">
							</div>
						</div>
					</div>
					<h3><span class="label label-default">Average</span></h3>
					<div class="form-inline">
						<div class="form-group" style="width:50%">
							<label for="txtavgQTY" class="col-sm-2 control-label">QTY</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtavgQTY" placeholder="QTY">
							</div>
						</div>

						<div class="form-group" style="width:50%">
							<label for="txtavgRPH" class="col-sm-2 control-label">RPH</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtavgRPH" placeholder="RPH">
							</div>
						</div>
					</div>
					<hr />
					<h3><span class="label label-default">Target</span></h3>
					<div class="form-inline">
						<div class="form-group" style="width:50%">
							<label for="txttrgtQTY" class="col-sm-2 control-label">QTY</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txttrgtQTY" placeholder="QTY">
							</div>
						</div>

						<div class="form-group" style="width:50%">
							<label for="txttrgtRPH" class="col-sm-2 control-label">RPH</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txttrgtRPH" placeholder="RPH">
							</div>
						</div>
					</div>
					<hr />
					<div class="form-group">
						<label for="txtKeterangan" class="col-sm-2 control-label">Keterangan</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtKeterangan" placeholder="Keterangan">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="btnSaveWeekly" class="btn btn-primary">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->