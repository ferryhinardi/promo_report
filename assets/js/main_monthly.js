$(function(){
	$.ajax({
		url: base_url + 'index.php/monthly/getFilterPeriod',
		dataType: 'JSON',
		async: false
	}).done(function(data, textStatus, jqXHR){
		if (data.length){
			for (var i in data)
				$('#ddlPeriodMonthly').append('<option value='+data[i].id+'>'+data[i].period+'</option>');
		}
	});
	$('#ddlPeriodMonthly').change(function(e) {
		var Data = {};
		Data['printType'] = 'html';
		Data['id'] = $(this).val();
		$.ajax({
			url: base_url + 'index.php/bak_generateReport',
			data: JSON.stringify(Data),
			beforeSend: function( xhr ) {
				$("#Workspace").html('');
				loadingStart();
			},
			success: function(e) {
				loadingFinish();
				if(e=="Page invalid")
				{
					e="The Report is Empty";
				}
				$("#Workspace").html(e.Report);
				if (typeof e.imageuri != 'undefined')
					$('img').attr('src', e.imageuri);
				else
					$('img').attr('src', base_url + 'assets/img/logo.jpg');
			},
			dataType: 'json',
			contentType: 'application/json;charset=utf-8',
			type: 'POST'
		});
	});
	$('#ddlPeriodMonthly').trigger('change');
});

function printMonthly(element)
{
	var printType = $(element).attr('id');
	window.open('bak_generateReport/download?'+printType+'&'+$('#ddlPeriodMonthly').val(),'_blank');
}