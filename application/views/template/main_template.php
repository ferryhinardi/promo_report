<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		
		<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/picker/css/datepicker.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/datatable/css/dataTables.bootstrap.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/mCustomScrollbar/jquery.mCustomScrollbar.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css" type="text/css" media="screen">

		<script src="<?php echo base_url();?>assets/lib/js/jquery-2.1.1.min.js" type="text/javascript" language="javascript"></script>
		<script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>
		<script src="<?php echo base_url();?>assets/lib/picker/js/bootstrap-datepicker.js" type="text/javascript" language="javascript"></script>
		<script src="<?php echo base_url();?>assets/lib/datatable/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"></script>
		<script src="<?php echo base_url();?>assets/lib/datatable/js/dataTables.bootstrap.js" type="text/javascript" language="javascript"></script>
		<script src="<?php echo base_url();?>assets/lib/mCustomScrollbar/jquery.mCustomScrollbar.js" type="text/javascript" language="javascript"></script>
		<script src="<?php echo base_url();?>assets/js/main.js" type="text/javascript" ></script>
		<script src="<?php echo base_url();?>assets/js/main_monthly.js" type="text/javascript" ></script>
		<script type="text/javascript">
			var base_url = "<?php echo base_url(); ?>";
			var displayname = "<?php echo $name ?>";
			var company = "<?php echo $company ?>";
		</script>
	</head>
	<body>
		<div class="container">
			<?= (isset($header))?$header:''; ?>
			<?= (isset($pageContent))?$pageContent:''; ?>
		</div>
		<?php $this->load->view('partial/alert') ?>
	</body>
</html>