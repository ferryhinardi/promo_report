<?php 
	class GenerateReport extends CI_Controller {
		public function index()
		{
			$url = explode('/',$_SERVER['REQUEST_URI'])[3];
			$param = explode('?', $url)[1];
			$expl = explode('&', $param);
			$reportType = $expl[0];
			$iddeadlineweekly = $expl[1];
			require_once ('jasper_client/rest/client/JasperClient.php');
			$client = new Jasper\JasperClient('localhost',			// Hostname : 192.168.0.105
									8080,							// Port
									'jasperadmin',					// Username
									'jasperadmin',					// Password
									'/jasperserver',				// Base URL
									'organization_1');				// Organization (pro only)
			$division = $this->session->userdata('is_logged_in')[0]['division'];
			$Report_url = '/reports/Promo_Report/WeeklyReport';
			$report = $client->runReport($Report_url, $reportType, null, null, array('division' => $division, 'iddeadlineweekly' => $iddeadlineweekly));
			echo $report;
		}
	}
?>