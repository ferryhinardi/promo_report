<?php
class Report_m extends CI_Model 
{
	function getAllWeeklyData($division, $iddeadlineweekly)
	{
		$query = $this->db->query('CALL sp_getWeeklyData(?, ?)',array($division, $iddeadlineweekly));
		return $query->result_array();
	}

	function getDeadline($iddeadlineweekly)
	{
		$query = $this->db->query('CALL sp_getDeadline(?)',array($iddeadlineweekly));
		return $query->result_array();	
	}

	function getFilterPeriod()
	{
		$query = $this->db->query('CALL sp_getPeriodeWeekly()',array());
		return $query->result_array();	
	}

	function insertWeeklyData($data, $division)
	{
		$master = $this->load->database('default', true);
		$master->trans_start(TRUE);
		$arrayData = array(
			'idweeklyParam' => isset($data['idweekly']) ? $data['idweekly'] : null,
			'existingperiodParam' => isset($data['existingperiod']) ? $data['existingperiod'] : null,
			'noParam' => isset($data['no']) ? $data['no'] : null,
			'pluParam' => isset($data['plu']) ? $data['plu'] : null,
			'deskripsiParam' => isset($data['deskripsi']) ? $data['deskripsi'] : null,
			'jtdParam' => isset($data['jtd']) ? $data['jtd'] : null,
			'hppParam' => isset($data['hpp']) ? $data['hpp'] : null,
			'ppn_hppParam' => isset($data['ppn_hpp']) ? $data['ppn_hpp'] : null,
			'harga_jualParam' => isset($data['harga']) ? $data['harga'] : null,
			'ratioParam' => isset($data['ratio']) ? $data['ratio'] : null,
			'edlpParam' => isset($data['edlp']) ? $data['edlp'] : null,
			'hebohParam' => isset($data['heboh']) ? $data['heboh'] : null,
			'iklanParam' => isset($data['iklan']) ? $data['iklan'] : null,
			'hematParam' => isset($data['hemat']) ? $data['hemat'] : null,
			'hthParam' => isset($data['hth']) ? $data['hth'] : null,
			'cardParam' => isset($data['card']) ? $data['card'] : null,
			'harga_promoParam' => isset($data['harga_promo']) ? $data['harga_promo'] : null,
			'alokasiParam' => isset($data['alokasi']) ? $data['alokasi'] : null,
			'spdParam' => isset($data['spd']) ? $data['spd'] : null,
			'dsiParam' => isset($data['dsi']) ? $data['dsi'] : null,
			'avg_qtyParam' => isset($data['avg_qty']) ? $data['avg_qty'] : null,
			'avg_rphParam' => isset($data['avg_rph']) ? $data['avg_rph'] : null,
			'target_qtyParam' => isset($data['target_qty']) ? $data['target_qty'] : null,
			'target_rphParam' => isset($data['target_rph']) ? $data['target_rph'] : null,
			'keteranganParam' => isset($data['keterangan']) ? $data['keterangan'] : null,
			'iddeadlineweeklyParam' => isset($data['iddeadlineweekly']) ? $data['iddeadlineweekly'] : null,
			'divisionParam' => $division);
		
		$master->query('CALL sp_insertWeeklyData(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $arrayData);
		$master->trans_complete();
		if($master->trans_status() === FALSE)
		{
			// do something if it fails
			$master->trans_rollback();
		}
		else
		{
			$master->trans_commit();
		}
		
		return $master->trans_status();
	}

	function deleteWeeklyData($idweekly){
		$query = $this->db->query('CALL sp_deleteWeeklyData(?)',array($idweekly));
		return $query->result_array();
	}
}

?>