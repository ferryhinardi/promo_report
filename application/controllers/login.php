<?php 
class Login extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		if($this->Login_m->is_logged_in())
		{
			redirect('home');
		}
		else
		{
			$this->form_validation->set_rules('username', 'lang:login_undername', 'callback_login_check');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if($this->form_validation->run() == FALSE)
			{
				$this->load->view('login');
			}
			else
			{
				redirect('home');
			}
		}
	}
	
	public function do_login(){
		$post = $this->input->post();
		$result = $this->Login_m->login($post['username'], md5($post['password']));
		if (count($result) > 0){
			$this->session->set_userdata('is_logged_in', $result);
		}
		$this->output->set_output(json_encode($result));
	}

	public function do_logout() {
		$this->session->sess_destroy();
		redirect("login");
	}
}