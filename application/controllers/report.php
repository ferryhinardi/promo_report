<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function getDeadline()
	{
		if(!$this->Login_m->is_logged_in()) redirect('login');
		$this->load->model('Report_m', 'report');
		$post = $this->input->post();
		$iddeadlineweekly = isset($post['iddeadlineweekly']) ? $post['iddeadlineweekly'] : null;
		$result = $this->report->getDeadline($iddeadlineweekly);
		$this->output->set_output(json_encode($result));
	}

	public function getFilterPeriod()
	{
		if(!$this->Login_m->is_logged_in()) redirect('login');
		$this->load->model('Report_m', 'report');
		$result = $this->report->getFilterPeriod();
		$this->output->set_output(json_encode($result));
	}

	public function getAllWeeklyData()
	{
		if(!$this->Login_m->is_logged_in()) redirect('login');
		$this->load->model('Report_m', 'report');
		$post = $this->input->post();
		$division = $this->session->userdata('is_logged_in')[0]['division'];
		$periode = isset($post['periode']) ? $post['periode'] : null;
		$result = $this->report->getAllWeeklyData($division, $periode);
		foreach ($result as $key => $value) {
			# code...
			$result[$key] = $value;
		}
		$this->output->set_output(json_encode($result));
	}

	public function addWeeklyData()
	{
		$post = $this->input->post()['input'];
		if(!$this->Login_m->is_logged_in()) redirect('login');
		$this->load->model('Report_m', 'report');
		$division = $this->session->userdata('is_logged_in')[0]['division'];
		$result = $this->report->insertWeeklyData($post,$division);
		$this->output->set_output(json_encode($result));
	}

	public function deleteWeeklyData()
	{
		if(!$this->Login_m->is_logged_in()) redirect('login');
		$this->load->model('Report_m', 'report');
		$post = $this->input->post('idweekly');
		$result = $this->report->deleteWeeklyData($post);
		$this->output->set_output(json_encode($result));
	}
}