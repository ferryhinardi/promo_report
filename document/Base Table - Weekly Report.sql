SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `MDS` ;
CREATE SCHEMA IF NOT EXISTS `MDS` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `MDS` ;

-- -----------------------------------------------------
-- Table `MDS`.`BRAND`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`BRAND` ;

CREATE TABLE IF NOT EXISTS `MDS`.`BRAND` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDS`.`TAG`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`TAG` ;

CREATE TABLE IF NOT EXISTS `MDS`.`TAG` (
  `id` CHAR(1) NOT NULL COMMENT '	',
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDS`.`DIVISION`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`DIVISION` ;

CREATE TABLE IF NOT EXISTS `MDS`.`DIVISION` (
  `id` TINYINT NOT NULL,
  `alias` VARCHAR(2) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDS`.`DEPARTMENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`DEPARTMENT` ;

CREATE TABLE IF NOT EXISTS `MDS`.`DEPARTMENT` (
  `id` VARCHAR(2) NOT NULL,
  `displayalias` VARCHAR(2) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(50) NOT NULL,
  `division` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_DEPARTMENT_DIVISION1_idx` (`division` ASC),
  CONSTRAINT `fk_DEPARTMENT_DIVISION1`
    FOREIGN KEY (`division`)
    REFERENCES `MDS`.`DIVISION` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDS`.`CATEGORY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`CATEGORY` ;

CREATE TABLE IF NOT EXISTS `MDS`.`CATEGORY` (
  `id` TINYINT NOT NULL,
  `department` VARCHAR(2) NOT NULL,
  `displayalias` VARCHAR(2) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `displayname` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`, `department`),
  INDEX `fk_CATEGORY_DEPARTMENT1_idx` (`department` ASC),
  CONSTRAINT `fk_CATEGORY_DEPARTMENT1`
    FOREIGN KEY (`department`)
    REFERENCES `MDS`.`DEPARTMENT` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDS`.`ITEM`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`ITEM` ;

CREATE TABLE IF NOT EXISTS `MDS`.`ITEM` (
  `plu` CHAR(10) NOT NULL,
  `department` VARCHAR(2) NOT NULL,
  `category` TINYINT NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  `tag` CHAR(1) NOT NULL,
  `brand` INT NOT NULL,
  `ibp` DECIMAL(15,4) NOT NULL,
  PRIMARY KEY (`plu`),
  INDEX `fk_ITEM_BRAND1_idx` (`brand` ASC),
  INDEX `fk_ITEM_TAG1_idx` (`tag` ASC),
  INDEX `fk_ITEM_CATEGORY1_idx` (`category` ASC, `department` ASC),
  CONSTRAINT `fk_ITEM_BRAND1`
    FOREIGN KEY (`brand`)
    REFERENCES `MDS`.`BRAND` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ITEM_TAG1`
    FOREIGN KEY (`tag`)
    REFERENCES `MDS`.`TAG` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ITEM_CATEGORY1`
    FOREIGN KEY (`category` , `department`)
    REFERENCES `MDS`.`CATEGORY` (`id` , `department`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDS`.`SPR`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`SPR` ;

CREATE TABLE IF NOT EXISTS `MDS`.`SPR` (
  `id` CHAR(1) NOT NULL,
  `name` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MDS`.`ISPR`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MDS`.`ISPR` ;

CREATE TABLE IF NOT EXISTS `MDS`.`ISPR` (
  `item` CHAR(10) NOT NULL COMMENT '	',
  `spr` CHAR(1) NOT NULL,
  `value` DECIMAL(10,0) NOT NULL,
  INDEX `fk_table1_ITEM1_idx` (`item` ASC),
  INDEX `fk_table1_SPR1_idx` (`spr` ASC),
  PRIMARY KEY (`spr`, `item`),
  CONSTRAINT `fk_table1_ITEM1`
    FOREIGN KEY (`item`)
    REFERENCES `MDS`.`ITEM` (`plu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_SPR1`
    FOREIGN KEY (`spr`)
    REFERENCES `MDS`.`SPR` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE = '';
GRANT USAGE ON *.* TO md;
 DROP USER md;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'md' IDENTIFIED BY 'md_2101';

GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE MDS.* TO 'md';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE MDP.* TO 'md';
GRANT UPDATE, SELECT, INSERT, DELETE ON TABLE MDTT.* TO 'md';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `MDS`.`TAG`
-- -----------------------------------------------------
START TRANSACTION;
USE `MDS`;
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('T', 'Temporary');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('H', 'Habiskan');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('R', 'Retur ke DC');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('O', 'Tidak boleh jual');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('N', 'Retur ke Supplier');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES (' ', 'Blank');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('L', 'Unknown');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('S', 'Unknown');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('D', 'Unknown');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('F', 'Unknown');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('I', 'Unknown');
INSERT INTO `MDS`.`TAG` (`id`, `name`) VALUES ('E', 'Unknown');

COMMIT;


-- -----------------------------------------------------
-- Data for table `MDS`.`SPR`
-- -----------------------------------------------------
START TRANSACTION;
USE `MDS`;
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('A', 'Harga A');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('B', 'Harga B');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('C', 'Harga C');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('D', 'Harga D');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('E', 'Harga E');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('F', 'Harga F');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('G', 'Harga G');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('H', 'Harga H');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('I', 'Harga I');
INSERT INTO `MDS`.`SPR` (`id`, `name`) VALUES ('J', 'Harga J');

COMMIT;

