-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2014 at 06:12 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `master`
--
CREATE DATABASE IF NOT EXISTS `master` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `master`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteWeeklyData`(idweeklyParam INT)
BEGIN
	DELETE FROM master.weekly
    WHERE idweekly = idweeklyParam;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getDeadline`(iddeadlineweeklyParam INT)
BEGIN
	SELECT *, DATE_FORMAT(deadline,'%W, %d %M %Y') AS DATE, DATE_FORMAT(deadline,'%H:%i') AS TIME, 
    CASE WHEN deadline < SYSDATE() THEN 1 ELSE 0 END AS is_lock
    FROM master.deadlineweekly
    WHERE iddeadlineweekly = iddeadlineweeklyParam
    ORDER BY deadline DESC
    LIMIT 1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getPeriodeWeekly`()
BEGIN
	SELECT iddeadlineweekly, periode 
    FROM master.deadlineweekly
    ORDER BY status, deadline;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getPeriodMonthly`()
BEGIN
	SELECT DISTINCT idperiod AS id, period FROM mdclm.claim;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getWeeklyData`(division INT, iddeadlineweekly INT)
BEGIN
	SELECT a.*
    FROM master.weekly a
    WHERE a.division = division
    AND a.iddeadlineweekly = iddeadlineweekly;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertWeeklyData`(
	idweeklyParam INT,
    existingperiodParam TEXT,
    noParam TINYINT,
    pluParam CHAR(8),
    deskripsiParam TEXT,
    jtdParam INT,
    hppParam INT,
    ppn_hppParam INT,
    harga_jualParam INT,
    ratioParam INT,
    edlpParam INT,
    hebohParam INT,
	iklanParam INT,
	hematParam INT,
	hthParam INT,
	cardParam INT,
	harga_promoParam INT,
	alokasiParam FLOAT,
	spdParam FLOAT,
	dsiParam FLOAT,
	avg_qtyParam DECIMAL,
	avg_rphParam INT,
	target_qtyParam DECIMAL,
	target_rphParam INT,
	keteranganParam TEXT,
    iddeadlineweeklyParam INT,
	divisionParam INT
)
BEGIN
	DECLARE has_error INT;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET has_error = 1;
    
    IF (idweeklyParam IS NOT NULL AND idweeklyParam <> 0) 
    THEN
		BEGIN
			IF EXISTS (SELECT idweekly FROM master.weekly WHERE idweekly = idweeklyParam)
            THEN
				BEGIN
					-- UPDATE DATA weekly
                    UPDATE master.weekly
                    SET 
						existingperiod = existingperiodParam,
						no = noParam,
						plu = pluParam,
						deskripsi = deskripsiParam,
						jtd = jtdParam,
						hpp = hppParam,
						ppn_hpp = ppn_hppParam,
						harga = harga_jualParam,
						ratio = ratioParam,
						edlp = edlpParam,
						heboh = hebohParam,
						iklan = iklanParam,
						hemat = hematParam,
						hth = hthParam,
						card = cardParam,
						harga_promo = harga_promoParam,
						alokasi = alokasiParam,
						spd = spdParam,
						dsi = dsiParam,
						avg_qty = avg_qtyParam,
						avg_rph = avg_rphParam,
						target_qty = target_qtyParam,
						target_rph = target_rphParam,
						keterangan = keteranganParam,
						division = divisionParam,
                        iddeadlineweekly = iddeadlineweeklyParam,
                        datein = NOW()
                    WHERE idweekly = idweeklyParam;
                END;
			END IF;
        END;
	ELSE
		BEGIN
			SELECT @idweeklyParam := COALESCE(MAX(idweekly),0) + 1 AS NewIdWeekly FROM master.weekly;
			-- INSERT DATA weekly
            
			INSERT INTO master.weekly (
				idweekly,
				existingperiod,
				no,
				plu,
				deskripsi,
				jtd,
				hpp,
				ppn_hpp,
				harga,
				ratio,
				edlp,
				heboh,
				iklan,
				hemat,
				hth,
				card,
				harga_promo,
				alokasi,
				spd,
				dsi,
				avg_qty,
				avg_rph,
				target_qty,
				target_rph,
				keterangan,
				division,
                iddeadlineweekly,
				datein
			)
			VALUES(
				@idweeklyParam,
				existingperiodParam, 
				noParam,
				pluParam,
				deskripsiParam,
				jtdParam,
				hppParam,
				ppn_hppParam,
				harga_jualParam,
				ratioParam,
				edlpParam,
				hebohParam,
				iklanParam,
				hematParam,
				hthParam,
				cardParam,
				harga_promoParam,
				alokasiParam,
				spdParam,
				dsiParam,
				avg_qtyParam,
				avg_rphParam,
				target_qtyParam,
				target_rphParam,
				keteranganParam,
				divisionParam,
                iddeadlineweeklyParam,
				NOW()
			);
            
		END;
	END IF;
/*================================================================	
CALL sp_insertWeeklyData(2,'28 Nov - 2 Des',1,'ABCD1235','Test_desc2',20,20000,2000,8000,1.0,8000,2000,100,1000,5000,8000,4000,4.5,4.5,3.5,2.0,20,20,20,'Test_keterangan2',1);
SET SQL_SAFE_UPDATES = 0;
UPDATE master.weekly SET datein = NOW();
================================================================*/
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login_system`(username VARCHAR(100), password VARCHAR(100))
BEGIN
	SELECT * FROM master.login a 
		JOIN mds.division b ON a.division = b.id
	WHERE a.username = username AND a.password = password;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `app_config`
--

CREATE TABLE IF NOT EXISTS `app_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_config`
--

INSERT INTO `app_config` (`key`, `value`) VALUES
('company', 'KOMPAS'),
('timezone', 'America/New_York');

-- --------------------------------------------------------

--
-- Table structure for table `deadlineweekly`
--

CREATE TABLE IF NOT EXISTS `deadlineweekly` (
  `iddeadlineweekly` int(11) NOT NULL,
  `deadline` datetime NOT NULL,
  `status` char(1) NOT NULL COMMENT '''A'' => Active; ''D'' =>Deactive',
  `periode` varchar(50) NOT NULL,
  PRIMARY KEY (`iddeadlineweekly`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deadlineweekly`
--

INSERT INTO `deadlineweekly` (`iddeadlineweekly`, `deadline`, `status`, `periode`) VALUES
(1, '2014-11-20 12:00:00', 'A', '7 November - 10 November'),
(2, '2014-11-10 15:00:00', 'D', '1 November - 3 November');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `division` tinyint(4) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FK_Division` (`division`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`user_id`, `username`, `password`, `division`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `reportclaimdetail_vw`
--
CREATE TABLE IF NOT EXISTS `reportclaimdetail_vw` (
`claim` varchar(30)
,`promotion` varchar(10)
,`description` varchar(200)
,`proposal` varchar(100)
,`start` date
,`end` date
,`deposit` decimal(15,4)
,`usage` decimal(15,4)
,`diff` decimal(15,4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `reportclaim_vw`
--
CREATE TABLE IF NOT EXISTS `reportclaim_vw` (
`id` varchar(30)
,`no` varchar(200)
,`name` varchar(50)
,`up` varchar(100)
,`title` varchar(100)
,`idperiod` int(11)
,`period` varchar(30)
,`payment_method` varchar(100)
,`name1` varchar(100)
,`job1` varchar(100)
,`name2` varchar(100)
,`job2` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `weekly`
--

CREATE TABLE IF NOT EXISTS `weekly` (
  `idweekly` int(11) NOT NULL,
  `existingperiod` text CHARACTER SET latin1,
  `no` tinyint(4) DEFAULT NULL,
  `plu` char(8) CHARACTER SET latin1 DEFAULT NULL,
  `deskripsi` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `jtd` int(11) DEFAULT NULL,
  `hpp` int(11) DEFAULT NULL,
  `ppn_hpp` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `ratio` float DEFAULT NULL,
  `edlp` int(11) DEFAULT NULL,
  `heboh` int(11) DEFAULT NULL,
  `iklan` int(11) DEFAULT NULL,
  `hemat` int(11) DEFAULT NULL,
  `hth` int(11) DEFAULT NULL,
  `card` int(11) DEFAULT NULL,
  `harga_promo` int(11) DEFAULT NULL,
  `alokasi` float DEFAULT NULL,
  `spd` float DEFAULT NULL,
  `dsi` float DEFAULT NULL,
  `avg_qty` decimal(10,0) DEFAULT NULL,
  `avg_rph` int(11) DEFAULT NULL,
  `target_qty` decimal(10,0) DEFAULT NULL,
  `target_rph` int(11) DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1,
  `division` int(11) DEFAULT NULL,
  `iddeadlineweekly` int(11) DEFAULT NULL,
  `datein` datetime DEFAULT NULL,
  PRIMARY KEY (`idweekly`),
  KEY `FK_DeadlineWeekly_idx` (`iddeadlineweekly`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `weekly`
--

INSERT INTO `weekly` (`idweekly`, `existingperiod`, `no`, `plu`, `deskripsi`, `jtd`, `hpp`, `ppn_hpp`, `harga`, `ratio`, `edlp`, `heboh`, `iklan`, `hemat`, `hth`, `card`, `harga_promo`, `alokasi`, `spd`, `dsi`, `avg_qty`, `avg_rph`, `target_qty`, `target_rph`, `keterangan`, `division`, `iddeadlineweekly`, `datein`) VALUES
(2, '20 Nov - 28 Nov', 1, 'ABCD1235', 'Test_desc2', 20, 20000, 2000, 8000, 1, 8000, 2000, 100, 1000, 5000, 8000, 4000, 5, 4.5, 1.25, '2', 20, '100', 400000, 'Test_keterangan2', 1, 1, '2014-11-14 19:22:33'),
(3, 'SH 16 - 30 NOVEMBER', 1, '10004715', 'PEPSODENT WHITE JUMBO 190G', 5000, 8000, 8800, 9100, 0, 0, 0, 0, 600, 0, 0, 9100, 4, 1, 4, '300', 27000000, '20000', 182000000, '', 1, 1, '2014-11-14 21:20:59'),
(4, 'SH25Nov', 127, 'ABCDE123', 'Test', 20, 30000, 33000, 200000, 0, 3000, 3000, 3000, 3000, 3000, 3000, 182000, 20, 20, 1, '100', 100, '400', 72800000, 'Test Keterangan', 1, 2, '2014-11-20 00:53:25');

-- --------------------------------------------------------

--
-- Structure for view `reportclaimdetail_vw`
--
DROP TABLE IF EXISTS `reportclaimdetail_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reportclaimdetail_vw` AS select `mdclm`.`mechanism`.`claim` AS `claim`,`mdclm`.`mechanism`.`promotion` AS `promotion`,`mdclm`.`mechanism`.`description` AS `description`,`mdclm`.`mechanism`.`proposal` AS `proposal`,`mdclm`.`mechanism`.`start` AS `start`,`mdclm`.`mechanism`.`end` AS `end`,`mdclm`.`mechanism`.`deposit` AS `deposit`,`mdclm`.`mechanism`.`usage` AS `usage`,`mdclm`.`mechanism`.`diff` AS `diff` from `mdclm`.`mechanism`;

-- --------------------------------------------------------

--
-- Structure for view `reportclaim_vw`
--
DROP TABLE IF EXISTS `reportclaim_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reportclaim_vw` AS select `a`.`id` AS `id`,`a`.`no` AS `no`,`d`.`name` AS `name`,`a`.`up` AS `up`,`a`.`title` AS `title`,`a`.`idperiod` AS `idperiod`,`a`.`period` AS `period`,`e`.`name` AS `payment_method`,`a`.`name1` AS `name1`,`a`.`job1` AS `job1`,`a`.`name2` AS `name2`,`a`.`job2` AS `job2` from ((((`mdclm`.`claim` `a` join `mdclm`.`supplier` `b` on((`a`.`claimed` = `b`.`id`))) join `mdclm`.`asupplier` `c` on((`b`.`id` = `c`.`alias`))) join `mds`.`supplier` `d` on((`c`.`supplier` = `d`.`id`))) join `mdclm`.`paymentmethod` `e` on((`a`.`pm` = `e`.`id`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`division`) REFERENCES `mds`.`division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `weekly`
--
ALTER TABLE `weekly`
  ADD CONSTRAINT `FK_DeadlineWeekly` FOREIGN KEY (`iddeadlineweekly`) REFERENCES `deadlineweekly` (`iddeadlineweekly`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
